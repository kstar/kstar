#include <cstdint>
#include <string>
#include <queue>

#include "GenUtils.h"
#include "Printer.h"

#undef KSTAR_DBG

using namespace clang;

VarDecl *GenUtils::getIterDecl(ForStmt *cur_for) {
  Stmt *ini = cur_for->getInit();
  /* case : for(int i = 2, ...*/
  if (isa<DeclStmt>(ini))
    return cast<VarDecl>(cast<DeclStmt>(ini)->getSingleDecl());
  /* case : for(i = 2, ...*/
  else if (isa<BinaryOperator>(ini)) {
    BinaryOperator *binary = cast<BinaryOperator>(ini);
    ValueDecl *ValD = cast<DeclRefExpr>(binary->getLHS())->getDecl();
    return dyn_cast_or_null<VarDecl>(ValD);
  } else
    assert(false && "Unsupported iterator inside a loop statement!");
}

unsigned GenUtils::getCollapsedNumber(OMPExecutableDirective *S) {
  unsigned CollapsedNum = 1;
  if (isa<OMPForDirective>(S))
    CollapsedNum = cast<OMPForDirective>(S)->getCollapsedNumber();
  if (isa<OMPParallelForDirective>(S))
    CollapsedNum = cast<OMPParallelForDirective>(S)->getCollapsedNumber();

  // XXX: Get rid of this workaround!
  if (!CollapsedNum)
    CollapsedNum = 1;

  return CollapsedNum;
}

std::string GenUtils::getAddrVar(VarDecl const *param, DataSharingInfo &dsi,
                                 OMPExecutableDirective *ompd) {
  std::string resultString;
  if (dsi.isShared(ompd, param) == dsi.wasShared(ompd, param))
    resultString = "&";
  resultString += param->getName();
  return resultString;
}

std::string GenUtils::getVar(VarDecl const *param, DataSharingInfo &dsi,
                             OMPExecutableDirective *ompd, bool isLastPrivate) {
  std::string resultString;
  bool isAddress = (dsi.isShared(ompd, param) || isLastPrivate ||
                    dsi.isReduce(ompd, param) || dsi.isCopyIn(ompd, param) ||
                    (dsi.isFirstPrivate(ompd, param) &&
                     (isa<OMPParallelDirective>(ompd) ||
                      isa<OMPParallelSectionsDirective>(ompd) ||
                      isa<OMPParallelForDirective>(ompd))));
  const bool wasSharedSkipInline = dsi.wasSharedSkipInline(ompd, param);

#ifdef KSTAR_DBG
  resultString = " /* var status: ";
  resultString += isAddress?"isAddress |":"!isAddress |";
  resultString += wasSharedSkipInline?"wasSharedSkipInline |":"!wasSharedSkipInline |";
  resultString += dsi.isShared(ompd, param)?"isShared |":"!isShared |";
  resultString += dsi.isFirstPrivate(ompd, param)?"isFirstPrivate |":"!isFirstPrivate |";
  resultString += " */ ";
#else
  resultString = "";
#endif

  if (isAddress) {
    if (!wasSharedSkipInline) {
      resultString += "&";
    }
  } else if (wasSharedSkipInline) {
    resultString += "*";
  }
  resultString += param->getName();
  return resultString;
}

QualType GenUtils::getDowncastedType(const VarDecl *VD, DataSharingInfo &DSI,
                                     OMPExecutableDirective *S,
                                     ASTContext &Context, bool isLastPrivate) {
  QualType Ty =
    VD->getType().getNonReferenceType().withoutLocalFastQualifiers();
  QualType PtrTy = Ty;
  int PtrDepth = 0;

  // Get the innermost element type which is not a pointer.
  while (PtrTy->isPointerType()) {
    PtrTy = PtrTy->getPointeeType();
    PtrDepth++;
  }

  if (PtrTy->isVariableArrayType()) {
    // Return the innermost element type of a type (which needn't actually
    // be an array type).
    Ty = Context.getBaseElementType(PtrTy);

    for (int i = 0; i < PtrDepth + 1; i++)
      Ty = Context.getPointerType(Ty);
  } else {
    if (!Ty->isDependentType()) {
      // Get the canonical type only when it's not a dependent type, meaning
      // that its definition doesn't depend on a template parameter.
      Ty = Ty.getCanonicalType();
    }

    if (DSI.isShared(S, VD) || isLastPrivate ||
        DSI.isReduce(S, VD) || DSI.isCopyIn(S, VD) ||
        (DSI.isFirstPrivate(S, VD) &&
         (isa<OMPParallelDirective>(S) ||
          isa<OMPParallelForDirective>(S) ||
          isa<OMPParallelSectionsDirective>(S)))) {
      Ty = Context.getPointerType(Ty);
    }

  }

  return Ty;
}

std::string GenUtils::asParamDownType(const VarDecl *VD,
                                      DataSharingInfo &DSI,
                                      OMPExecutableDirective *S,
                                      ASTContext &Context, bool varName,
                                      bool isLastPrivate) {
  QualType Ty = getDowncastedType(VD, DSI, S, Context, isLastPrivate);
  std::string realName;
  if (varName) {
    if (isLastPrivate) {
      realName = "__kstar__fp_";
    }
    realName += VD->getName();
  }

  std::string str;
  llvm::raw_string_ostream rso(str);
  Ty.print(rso, Context.getPrintingPolicy(), realName);

  return rso.str();
}

std::string GenUtils::asCastDownType(VarDecl const *VD, DataSharingInfo &dsi,
                                     OMPExecutableDirective *ompd,
                                     ASTContext &ctx, bool isLastPrivate) {
  return "(" + asParamDownType(VD, dsi, ompd, ctx, false, isLastPrivate) + ")";
}

std::string GenUtils::asParamType(VarDecl const *param, DataSharingInfo &dsi,
                                  OMPExecutableDirective *ompd, ASTContext &ctx,
                                  bool varName) {
  QualType pointeType =
      param->getType().getNonReferenceType().withoutLocalFastQualifiers();
  int pointeeDepth = 0;
  while (pointeType->isPointerType()) {
    pointeType = pointeType->getPointeeType();
    pointeeDepth++;
  }

  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string realName;
  if (varName)
    realName = param->getName();
  if (pointeType->isArrayType()) {
    for (int i = 0; i < pointeeDepth; i++)
      realName = "(*" + realName + ")";
    if (dsi.isShared(ompd, param) ||
        (isa<OMPTaskDirective>(ompd) && dsi.isFirstPrivate(ompd, param)))
      realName = "(*" + realName + ")";
    if (pointeType->isVariableArrayType()) {
      if (dsi.isCopyIn(ompd, param))
        realName = "(*" + realName + ")";
      while (pointeType->isVariableArrayType()) {
        QualType arrayType = pointeType;
        pointeType = pointeType->getAsArrayTypeUnsafe()->getElementType();
        realName = " " + realName + "[";
        Expr *size = cast<VariableArrayType>(arrayType->getAsArrayTypeUnsafe())
                         ->getSizeExpr();
        std::string dummy;
        llvm::raw_string_ostream dummys(dummy);
        std::queue<std::string> dummy_queue;
        KStarStmtPrinter p(dummys, ctx.getPrintingPolicy(), dummy_queue, &dsi,
                           ompd);
        p.PrintExpr(size);
        realName += dummys.str();
        realName += "]";
      }
      realName = pointeType.getAsString() + " " + realName;
      rso << realName;
    } else {
      QualType QT = pointeType.getDesugaredType(ctx);
      QT.print(rso, ctx.getPrintingPolicy(), realName);
    }
  } else {
    QualType QT =
        param->getType().getNonReferenceType().withoutLocalFastQualifiers();
    if (!QT->isDependentType())
      QT = QT.getCanonicalType();

    if (dsi.isShared(ompd, param) ||
        (isa<OMPTaskDirective>(ompd) && dsi.isFirstPrivate(ompd, param))) {
      QT = ctx.getPointerType(QT);
    }

    QT.print(rso, ctx.getPrintingPolicy(), realName);
  }

  return rso.str();
}

std::string GenUtils::asCastType(VarDecl const *VD, DataSharingInfo &dsi,
                                 OMPExecutableDirective *ompd,
                                 ASTContext &ctx) {
  return asParamType(VD, dsi, ompd, ctx, false);
}

const VarDecl *GenUtils::getVarDeclFromExpr(Expr *E) {
  const VarDecl *VD = nullptr;
  Expr *BaseExpr = E->IgnoreImplicit();

  if (isa<ArraySubscriptExpr>(BaseExpr)) {
    while (isa<ArraySubscriptExpr>(BaseExpr))
      BaseExpr = cast<ArraySubscriptExpr>(BaseExpr)->getBase()->IgnoreImplicit();
  }

  if (isa<OMPArraySectionExpr>(BaseExpr)) {
    while (isa<OMPArraySectionExpr>(BaseExpr))
      BaseExpr = cast<OMPArraySectionExpr>(BaseExpr)->getBase()->IgnoreImplicit();
  }

  if (isa<DeclRefExpr>(BaseExpr)) {
    ValueDecl *ValueD = cast<DeclRefExpr>(BaseExpr)->getDecl();
    if (ValueD)
      VD = cast<VarDecl>(ValueD);
  }

  return VD;
}

Type const *GenUtils::getRealElementType(QualType const &QT) {
  QualType canQT = QT.getCanonicalType();
  Type const *canType = canQT.getTypePtrOrNull();
  // get real type
  while (canType && canType->isPointerType())
    canType = canType->getPointeeType().getTypePtrOrNull();
  while (canType && canType->isArrayType())
    canType = cast<ArrayType>(canType)->getElementType().getTypePtrOrNull();
  while (canType && canType->isPointerType())
    canType = canType->getPointeeType().getTypePtrOrNull();
  return canType;
}

bool GenUtils::isPointerType(VarDecl const *var) {
  Type const *varType = var->getType().getTypePtrOrNull();
  return varType && varType->isPointerType();
}

void GenUtils::ReplaceSrcRange(Rewriter *r, SourceRange &refLoc,
                               std::string const &replacement) {
  unsigned int length = r->getRangeSize(refLoc);
  unsigned int replacementLength = replacement.length();
  /* Note: this seems dirty, but the clean way (getting token length, then
   * use the rewriter to Replace (refLoc, refLoc+length, replacement), only
   * works up to "length" char in the replacement string...
   **/

  if (replacementLength <= length)
    r->ReplaceText(refLoc, replacement);
  else {
    r->InsertTextBefore(refLoc.getBegin(),
                        replacement.substr(0, replacementLength - length));
    r->ReplaceText(refLoc.getBegin(), length,
                   replacement.substr(replacementLength - length, length));
  }
}

FunctionDecl const *GenUtils::getTopFunctionDecl(Stmt const *S,
                                                 ASTContext &C) {
  ASTContext::DynTypedNodeList ParentList = C.getParents(*S);
  while (1) {
    // Get first parent node.
    const ast_type_traits::DynTypedNode *Node = ParentList.begin();
    if (!Node)
      break;

    if (Node->get<FunctionDecl>()) {
      return Node->get<FunctionDecl>();
    } else {
      // While the current node is not a function, go up in the AST.
      if (Node->get<Stmt>()) {
        ParentList = C.getParents(*Node->get<Stmt>());
      } else {
        ParentList = C.getParents(*Node->get<Decl>());
      }
    }
  }
  assert(!"Failed to find the top function declaration!");
  return nullptr;
}

FunctionTemplateDecl *GenUtils::getTopFunctionTemplateDecl(Stmt const *S,
                                                           ASTContext &C) {
  return GenUtils::getTopFunctionDecl(S, C)->getDescribedFunctionTemplate();
}

ClassTemplateDecl *GenUtils::getTopClassTemplateDecl(Stmt const *S,
                                                     ASTContext &C) {
  const CXXMethodDecl *MD = GenUtils::getTopCXXMethodDecl(S, C);
  if (MD)
    return MD->getParent()->getDescribedClassTemplate();
  return nullptr;
}

CXXMethodDecl const *GenUtils::getTopCXXMethodDecl(Stmt const *S,
                                                   ASTContext &C) {
  const FunctionDecl *D = GenUtils::getTopFunctionDecl(S, C);
  return dyn_cast_or_null<CXXMethodDecl const>(D);
}

Decl const *GenUtils::getTopDecl(Stmt const *S, ASTContext &C) {
  Decl const *ParentDecl = getTopFunctionDecl(S, C);
  ASTContext::DynTypedNodeList ParentList = C.getParents(*ParentDecl);

  while (1) {
    // XXX: bleh
    for (const ast_type_traits::DynTypedNode *Node = ParentList.begin();
         Node != ParentList.end(); Node++) {
      if (Node->get<TranslationUnitDecl>())
        return ParentDecl;
      ParentDecl = Node->get<Decl>();
      ParentList = C.getParents(*ParentDecl);
      break;
    }
  }
  assert(!"Failed to find the top declaration!");
  return nullptr;
}

const std::string GenUtils::SourceRangeToString(const SourceRange &SR,
                                                ASTContext &C) {
  SourceLocation B = SR.getBegin();
  SourceLocation E = SR.getEnd();
  return std::string(C.getSourceManager().getCharacterData(B),
                     C.getSourceManager().getCharacterData(E) -
                         C.getSourceManager().getCharacterData(B));
}

std::string GenUtils::getForDecls(Stmt *capStmt, ASTContext &ctx, int depth) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  ForStmt *curFor = nullptr;
  for (int i = 0; i < depth; i++) {
    while (isa<CompoundStmt>(capStmt))
      capStmt = *cast<CompoundStmt>(capStmt)->body_begin();
    assert(isa<ForStmt>(capStmt) && "OMP For should contain ForStmt");
    curFor = cast<ForStmt>(capStmt);
    capStmt = curFor->getBody();

    Stmt *ini = curFor->getInit();
    if (isa<DeclStmt>(ini)) {
      VarDecl *var = cast<VarDecl>(cast<DeclStmt>(ini)->getSingleDecl());
      var->getType().print(rso, ctx.getPrintingPolicy(), var->getName());

      if (var->getType()->getContainedAutoType() && var->hasInit()) {
        // A variable declared with the 'auto' type (C++11) inside
        // a for statement has to be initialized in order to avoid
        // a compilation error in case the type cannnot be deduced,
        // especially when template parameters are used with it.
        if (isa<CXXUnresolvedConstructExpr>(var->getInit())) {
          CXXUnresolvedConstructExpr *E =
              cast<CXXUnresolvedConstructExpr>(var->getInit());

          // Initialize the variable with the default constructor
          // deduced from the original source code by the compiler.
          rso << " = ";
          E->getTypeAsWritten().print(rso, ctx.getPrintingPolicy());
          rso << "(";
          for (unsigned i = 0; i < E->arg_size(); i++) {
            if (i)
              rso << ",";
            E->getArg(i)->printPretty(rso, nullptr, ctx.getPrintingPolicy());
          }
          rso << ")";
        }
      }
      rso << ";\n";
    }
  }
  return rso.str();
}

QualType GenUtils::getThisType(OMPExecutableDirective *S, ASTContext &C,
                               DataSharingInfo &dsi) {
  const CXXMethodDecl *MD =
    dyn_cast_or_null<CXXMethodDecl const>(dsi.getFunctionDecl(S));
  return MD->getThisType(C);
}

std::string GenUtils::getTaskName(OMPExecutableDirective *S,
                                  ASTContext &C) {

  if (isa<OMPTaskDirective>(S)) {
    OMPTaskDirective *T = cast<OMPTaskDirective>(S);
    for (OMPClause *I : T->clauses()) {
      if (OMPTaskNameClause *C = dyn_cast_or_null<OMPTaskNameClause>(I)) {
        assert(dyn_cast_or_null<StringLiteral>(C->getName()));
        return dyn_cast_or_null<StringLiteral>(C->getName())->getString();
      }
    }
  }
  return std::string("UnnamedTask");
}

bool GenUtils::isLocalStructDecl(VarDecl const *D) {
  if (!D) {
    // In case of this pointer.
    return false;
  }

  // Make sure that the real element type of this variable is a struct.
  const Type *T = GenUtils::getRealElementType(D->getType());
  if (!T->isStructureType())
    return false;

  // A struct declaration is global when defined outside functions/methods.
  const RecordDecl *RD = T->getAsStructureType()->getDecl();
  if (RD->isDefinedOutsideFunctionOrMethod())
    return false;

  return true;
}

const std::string GenUtils::RecordDeclToStr(RecordDecl const *RD,
                                            const PrintingPolicy &Policy) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  // In case this record decl is actually declared as a typedef decl
  TypedefNameDecl *TND = RD->getTypedefNameForAnonDecl();
  if (TND)
    rso << "typedef ";

  rso << "struct " << RD->getName() << "\n{\n";
  for (auto *F : RD->fields()) {
    QualType QT = F->getType();
    if (!QT->isDependentType()) {
      // Do not try to reach the canonical type when it's a
      // dependent type, meaning that its definition somehow
      // depends on a template parameter.
      QT = QT.getCanonicalType();
    }
    QT.print(rso, Policy, F->getName());
    rso << ";\n";
  }
  rso << "}";

  if (TND)
    rso << " " << TND->getName();
  rso << ";";

  return rso.str();
}

const std::string GenUtils::ExprToStr(Expr const *E) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  rso << E;
  return rso.str();
}

const std::vector<VarDecl const *>
GenUtils::getCapturedVars(OMPExecutableDirective *OMPD) {
  CapturedStmt *S = cast<CapturedStmt>(OMPD->getAssociatedStmt());
  std::vector<VarDecl const *> Vars;

  for (auto &it : S->captures()) {
    if (it.capturesThis()) {
      // Special flags for 'this' pointer.
      Vars.insert(Vars.end(), nullptr);
    } else {
      auto kind = it.getCaptureKind();
      if (kind == CapturedStmt::VCK_VLAType)
        continue;

      const VarDecl *VD = it.getCapturedVar();
      if (!VD)
          continue;

      if (isa<OMPLoopDirective>(OMPD) && VD->isImplicit() &&
          VD->getName() == ".chunk.") {
        assert(VD->getInit());
        // Special case for chunksize variables to get a reference to the
        // real variable instead of the .chunk. implicit one.
        VD = GenUtils::getVarDeclFromExpr(const_cast<Expr *>(VD->getInit()));
      }

      // Special case for global size of VLAs because they are not captured
      // by the CapturedStmt object.
      QualType Ty = VD->getType();
      do {
        const ArrayType *arr;
        if (Ty->isPointerType())
          arr = Ty->getPointeeType()->getAsArrayTypeUnsafe();
        else
          arr = Ty->getAsArrayTypeUnsafe();

        if (!arr || !isa<VariableArrayType>(arr))
          break;

        Expr *SizeExpr = cast<VariableArrayType>(arr)->getSizeExpr();
        const VarDecl *SizeVD = GenUtils::getVarDeclFromExpr(SizeExpr);
        if (SizeVD) {
          if (std::find(Vars.begin(), Vars.end(), SizeVD) == Vars.end()) {
            // Do not duplicate VLAs global size.
            Vars.insert(Vars.end(), SizeVD);
          }
        }
        Ty = arr->getElementType();
      } while (Ty->hasSizedVLAType());

      if (!isa<OMPCapturedExprDecl>(VD)) {
        if (std::find(Vars.begin(), Vars.end(), VD) == Vars.end()) {
          Vars.insert(Vars.end(), VD);
        }
      }
    }
  }
  return Vars;
}

bool GenUtils::IsThreadPrivateVarDecl(OMPThreadPrivateDecl *OMPD, VarDecl *VD)
{
  for (auto it = OMPD->varlist_begin(); it != OMPD->varlist_end(); ++it) {
    if (isa<DeclRefExpr>(*it)) {
      if (VD == dyn_cast_or_null<VarDecl>(cast<DeclRefExpr>(*it)->getDecl()))
        return true;
    }
  }
  return false;
}
