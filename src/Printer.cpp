#include "Printer.h"
#include "GenUtils.h"
#include "codegen/Util.h"

using namespace clang;

#define GENERATE(PRAGMA)                                                       \
  void KStarStmtPrinter::VisitOMP##PRAGMA##Directive(                          \
      OMP##PRAGMA##Directive *s) {                                             \
    if (replacements_.size() > 0) {                                            \
      OS << replacements_.front();                                             \
      replacements_.pop();                                                     \
    }                                                                          \
  }
#include <pragma.def>

KStarStmtPrinter::KStarStmtPrinter(llvm::raw_ostream &os,
                                   const PrintingPolicy &Policy,
                                   std::queue<std::string> &q,
                                   DataSharingInfo *dsi,
                                   OMPExecutableDirective *omp_directive,
                                   bool parent, bool IsInner,
                                   std::vector<std::string> ByRef)
    : StmtPrinter(os, 0, Policy, 0), replacements_(q), dsi_(dsi),
      ompDirective_(omp_directive), parent_(parent), IsInner_(IsInner),
      ByRef_(ByRef), type_printer(new KStarTypePrinter(this)) {}

KStarStmtPrinter::~KStarStmtPrinter() { delete type_printer; }

void KStarStmtPrinter::VisitOMPAtomicDirective(OMPAtomicDirective *s) {
  OS << "\n";
  StmtPrinter::VisitOMPAtomicDirective(s);
}

void KStarStmtPrinter::VisitDeclRefExpr(DeclRefExpr *Node) {
  if (!dsi_ || !ompDirective_) {
    StmtPrinter::VisitDeclRefExpr(Node);
    return;
  }
  ValueDecl *ValD = Node->getDecl();
  VarDecl *VarD = dyn_cast_or_null<VarDecl>(ValD);
  /*A && B && C && ((!D && E) || (D && F))*/
  // We need to dereference captured var if:
  // - It is shared (Because we share variable between threads)
  //   . but is not a static data member, because cannot take the address of a static data member
  // - It has been shared (Because it is already shared between threads)
  // - It is firstprivate is task (Because copy have already be done at task
  // creation)
  // Seriously, how it's possible to write a check like that??
  bool AddDereference =
    (ompDirective_ && VarD && dsi_->isVarCaptured(ompDirective_, VarD) &&
      ((!parent_ && dsi_->isShared(ompDirective_, VarD) && (not VarD->isStaticDataMember())) ||
       (parent_ && dsi_->wasShared(ompDirective_, VarD) && (not VarD->isStaticDataMember())) ||
       (!parent_ && isa<OMPTaskDirective>(ompDirective_) &&
        dsi_->isFirstPrivate(ompDirective_, VarD)) ||
       (parent_ && dsi_->wasFirstPrivateTask(ompDirective_, VarD))));

  if (AddDereference) {
    if (std::find(ByRef_.begin(), ByRef_.end(),
                  VarD->getName()) != ByRef_.end())
      AddDereference = false;
  }

  if (AddDereference) {
    OS << "(*";
    StmtPrinter::VisitDeclRefExpr(Node);
    OS << ")";
  } else {
    llvm::APSInt Result;
    if (VarD && !dsi_->isVarCaptured(ompDirective_, VarD)
        && !VarD->hasGlobalStorage() && VarD->getType().isConstQualified()
        && Node->EvaluateAsInt(Result, VarD->getASTContext())) {
      // Because local const-qualified integer variables are not captured,
      // we need to directly write their values.
      OS << Result.toString(10);
    } else {
      StmtPrinter::VisitDeclRefExpr(Node);
    }
  }
}

void KStarStmtPrinter::VisitCXXNamedCastExpr(CXXNamedCastExpr *Node) {
  QualType QT = Node->getTypeAsWritten();
  if (!QT->isDependentType())
    QT = QT.getCanonicalType();

  OS << Node->getCastName() << '<';
  QT.print(OS, Policy);
  OS << ">(";
  PrintExpr(Node->getSubExpr());
  OS << ")";
}

void KStarStmtPrinter::VisitCXXStaticCastExpr(CXXStaticCastExpr *Node) {
  VisitCXXNamedCastExpr(Node);
}

void KStarStmtPrinter::VisitCXXDynamicCastExpr(CXXDynamicCastExpr *Node) {
  VisitCXXNamedCastExpr(Node);
}

void KStarStmtPrinter::VisitCXXReinterpretCastExpr(
    CXXReinterpretCastExpr *Node) {
  VisitCXXNamedCastExpr(Node);
}

void KStarStmtPrinter::VisitCXXConstCastExpr(CXXConstCastExpr *Node) {
  VisitCXXNamedCastExpr(Node);
}

void KStarStmtPrinter::VisitCXXFunctionalCastExpr(CXXFunctionalCastExpr *Node) {
  QualType QT = Node->getType();
  if (!QT->isDependentType())
    QT = QT.getCanonicalType();

  QT.print(OS, Policy);
  // If there are no parens, this is list-initialization, and the braces are
  // part of the syntax of the inner construct.
  if (Node->getLParenLoc().isValid())
    OS << "(";
  PrintExpr(Node->getSubExpr());
  if (Node->getLParenLoc().isValid())
    OS << ")";
}

void KStarStmtPrinter::PrintRawDecl(Decl *D) {
  KStarDeclPrinter DP(this, type_printer);
  DP.Visit(D);
}

void KStarStmtPrinter::VisitCXXThisExpr(CXXThisExpr *) {
  if (IsInner_)
    OS << "this";
  else
    OS << "kstar_this";
}

void KStarStmtPrinter::VisitCXXNewExpr(CXXNewExpr *E) {
  if (E->isGlobalNew())
    OS << "::";
  OS << "new ";
  unsigned NumPlace = E->getNumPlacementArgs();
  if (NumPlace > 0 && !isa<CXXDefaultArgExpr>(E->getPlacementArg(0))) {
    OS << "(";
    PrintExpr(E->getPlacementArg(0));
    for (unsigned i = 1; i < NumPlace; ++i) {
      if (isa<CXXDefaultArgExpr>(E->getPlacementArg(i)))
        break;
      OS << ", ";
      PrintExpr(E->getPlacementArg(i));
    }
    OS << ") ";
  }
  if (E->isParenTypeId())
    OS << "(";
  E->getAllocatedType().print(OS, Policy);
  if (Expr *Size = E->getArraySize()) {
    OS << '[';
    PrintExpr(Size);
    OS << ']';
  }
  if (E->isParenTypeId())
    OS << ")";

  CXXNewExpr::InitializationStyle InitStyle = E->getInitializationStyle();
  if (InitStyle) {
    if (InitStyle == CXXNewExpr::CallInit)
      OS << "(";
    PrintExpr(E->getInitializer());
    if (InitStyle == CXXNewExpr::CallInit)
      OS << ")";
  }
}

void KStarStmtPrinter::PrintRawDeclStmt(const DeclStmt *S) {
  KStarDeclPrinter DP(this, type_printer);
  DeclStmt::const_decl_iterator Begin = S->decl_begin(), End = S->decl_end();
  SmallVector<Decl *, 2> Decls;
  for (; Begin != End; ++Begin)
    Decls.push_back(*Begin);

  DP.printGroup(Decls.data(), Decls.size());
}

void KStarDeclPrinter::printGroup(Decl **Begin, unsigned NumDecls) {
  if (NumDecls == 1) {
    this->Visit(*Begin);
    return;
  }

  Decl **End = Begin + NumDecls;
  TagDecl *TD = dyn_cast<TagDecl>(*Begin);
  if (TD)
    ++Begin;

  if (TD && TD->isCompleteDefinition()) {
    TD->print(Out, Policy, Indentation);
  }

  bool isFirst = true;
  for (; Begin != End; ++Begin) {
    if (isFirst) {
      isFirst = false;
    } else {
      Out << ";\n";
    }
    this->Visit(*Begin);
  }
}

void KStarDeclPrinter::VisitVarDecl(VarDecl *D) {
  if (!Policy.SuppressSpecifiers) {
    StorageClass SC = D->getStorageClass();
    if (SC != SC_None)
      Out << VarDecl::getStorageClassSpecifierString(SC) << " ";
    Out << CodeGen::genTSCSpec(D->getTSCSpec()) << " ";
    if (D->isModulePrivate())
      Out << "__module_private__ ";
  }

  if (GenUtils::isLocalStructDecl(D)) {
    // Special case for local struct declarations.
    const Type *T = GenUtils::getRealElementType(D->getType());
    const RecordDecl *RD = T->getAsStructureType()->getDecl();
    Out << GenUtils::RecordDeclToStr(RD, Policy);
  }

  QualType T =
      D->getTypeSourceInfo()
          ? D->getTypeSourceInfo()->getType()
          : D->getASTContext().getUnqualifiedObjCPointerType(D->getType());

  if (!T->isDependentType())
    T = T.getCanonicalType();

  tp_->print(T, Out, D->getName());
  Expr *Init = D->getInit();
  if (!Policy.SuppressInitializers && Init) {
    bool ImplicitInit = false;
    if (CXXConstructExpr *Construct =
            dyn_cast<CXXConstructExpr>(Init->IgnoreImplicit())) {
      if (D->getInitStyle() == VarDecl::CallInit &&
          !Construct->isListInitialization()) {
        ImplicitInit = Construct->getNumArgs() == 0 ||
                       Construct->getArg(0)->isDefaultArgument();
      }
    }
    if (!ImplicitInit) {
      if ((D->getInitStyle() == VarDecl::CallInit) && !isa<ParenListExpr>(Init))
        Out << "(";
      else if (D->getInitStyle() == VarDecl::CInit) {
        Out << " = ";
      }
      /*Init->printPretty(Out, 0, Policy, Indentation);*/
      sp_->PrintExpr(Init);
      if ((D->getInitStyle() == VarDecl::CallInit) && !isa<ParenListExpr>(Init))
        Out << ")";
    }
  }
  prettyPrintAttributes(D);
}

void KStarTypePrinter::printVariableArrayAfter(const VariableArrayType *T,
                                               raw_ostream &OS) {
  OS << '[';
  if (T->getIndexTypeQualifiers().hasQualifiers()) {
    AppendTypeQualList(OS, T->getIndexTypeCVRQualifiers(), false /* Policy.LangOpts.C99 */);
    OS << ' ';
  }

  if (T->getSizeModifier() == VariableArrayType::Static)
    OS << "static";
  else if (T->getSizeModifier() == VariableArrayType::Star)
    OS << '*';

  if (T->getSizeExpr())
    sp_->PrintExpr(T->getSizeExpr());
  OS << ']';

  printAfter(T->getElementType(), OS);
}
