#include "RuntimePragma.h"

using namespace clang;

RuntimeBarrier::RuntimeBarrier(OMPBarrierDirective *t, ASTContext &c,
                               DataSharingInfo &dsa)
    : RuntimeDirective(t, c) {}

std::string RuntimeBarrier::genReplacePragma(std::queue<std::string> &) {
  return getRuntime()->EmitBarrier();
}
