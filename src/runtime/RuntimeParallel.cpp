#include "RuntimePragma.h"
#include "GenUtils.h"
#include "Printer.h"

#include "codegen/StructExpand.h"

using namespace clang;

RuntimeParallel::RuntimeParallel(OMPParallelDirective *ompd, ASTContext &ctx,
                                 DataSharingInfo &dsi)
    : RuntimeScopeDirective(ompd, ctx, dsi) {
}

std::string RuntimeParallel::genBeforeFunction(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << genDeclareArgStruct(argStruct_);

  if (!GenUtils::getTopCXXMethodDecl(getOMPDirective()->getAssociatedStmt(),
                                     getContext())) {
    // Only generate a signature when the given OpenMP pragma is
    // not located inside a CXX method.
    rso << getRuntime()->genTaskSignature(getContext(), newFunction_,
                                        addInjectedTemplateArgs(true)) << ";\n";
  }
  return rso.str();
}

std::string RuntimeParallel::genReplacePragma(std::queue<std::string> &) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  CodeGen::StructInit structVarAddr =
    genFillArgStruct("__kstar_args_" + std::to_string(getId()), argStruct_);
  rso << structVarAddr.str();

  std::string Name  = "_kstar_parallel_region_" + std::to_string(getId());
  std::string NumThreads = getNumThreads(numThreads_, ifClause_);
  std::string Func = newFunction_ + addInjectedTemplateArgs();
  std::string Arg = structVarAddr.getaddr();

  rso << getRuntime()->EmitParallelRegion(Name, NumThreads,
                                          procBind_, Func, Arg);

  return rso.str();
}

std::string RuntimeParallel::genFunctionBody(std::queue<std::string> &q,
                                             std::string const &params) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  CodeGen::StructExpand argStruct = expandArgStruct(params, argStruct_);
  rso << argStruct.str();
  if (copyIn_)
    rso << getRuntime()->EmitBarrier();

  // Write outlined function body changing for loop iteration space
  KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective());
  p.PrintStmt(captured_->getCapturedStmt());

  rso << genReductionCombiner(params, argStruct_);
  rso << argStruct.free();

  return rso.str();
}

std::string RuntimeParallel::genAfterFunction(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  // Create function declaration.
  FunctionDecl *FD =
    getRuntime()->CreateWrapperFunction(getContext(), newFunction_);

  // Create and print a template function declaration if needed.
  FunctionTemplateDecl *FTD = CreateFunctionTemplate(FD);
  if (FTD) {
    FTD->print(rso);
  } else {
    FD->print(rso);
  }

  // TODO: Set body using a Stmt object instead.
  rso << "{"
      << genFunctionBody(q, "kstar_params")
      << "}";

  return rso.str();
}
