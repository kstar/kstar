#include "RuntimePragma.h"
#include "GenUtils.h"
#include "Printer.h"

using namespace clang;

RuntimeParallelSections::RuntimeParallelSections(
    OMPParallelSectionsDirective *t, ASTContext &c, DataSharingInfo &dsi)
    : RuntimeScopeDirective(t, c, dsi)
/* OMP specification: num_thread==0, depend on the environment */
{
  wrapperFunction_ = newFunction_ + "_wrapper";
}

std::string
RuntimeParallelSections::genBeforeFunction(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string cl_arg = "params";
  rso << "for(unsigned long long __kstar__i__ = __kstar__begin__; __kstar__i__ "
         "< __kstar__j__; __kstar__i__++){\n";
  rso << "switch(__kstar__i__){\n";
  // OUTLINE EACH SECTION IN SECTIONS
  int num_sec = 0;
  CompoundStmt *body = cast<CompoundStmt>(captured_->getCapturedStmt());
  for (Stmt **b = body->body_begin(); b != body->body_end(); b++) {
    rso << "case " << num_sec++ << ":\n";
    // Write outlined function body changing for loop iteration space
    KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi,
                       getOMPDirective());
    p.PrintStmt(
        cast<CapturedStmt>(cast<OMPSectionDirective>(*b)->getAssociatedStmt())
            ->getCapturedStmt());
    rso << ";";
    // LAST PRIVATE
    if (num_sec == std::distance(body->body_begin(), body->body_end())) {
      for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
        if (dsi.isLastPrivate(getOMPDirective(), param)) {
          rso << "*((struct " << argStruct_ << "*)" << cl_arg
              << ")->__kstar__fp_" << param->getName();
          rso << " = " << param->getName() << ";\n";
        }
      }
    }
    rso << "break;\n";
  }
  rso << "}\n";
  rso << "}\n";

  std::string Body = rso.str();
  rso.str().clear();

  rso << genDeclareArgStruct(argStruct_)
      << genForOutlinedFunction(newFunction_, argStruct_, 1, Body, false);

  // Create function declaration.
  FunctionDecl *FD =
    getRuntime()->CreateWrapperFunction(getContext(), wrapperFunction_);

  // Print function declaration.
  FD->print(rso);

  OpenMPScheduleClauseKind ScheduleType =
    OpenMPScheduleClauseKind::OMPC_SCHEDULE_auto;

  // TODO: Set body using a Stmt object instead.
  rso << "{"
      << newFunction_ << "(" <<  std::to_string(num_sec)
      << ", kstar_params, 1, 1, "
      << getRuntime()->EmitScheduleType(ScheduleType)
      << ", 0, 0);"
      << "}";

  return rso.str();
}

std::string
RuntimeParallelSections::genReplacePragma(std::queue<std::string> &) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  CodeGen::StructInit structVarAddr =
    genFillArgStruct("__kstar_args_" + std::to_string(getId()), argStruct_);
  rso << structVarAddr.str();

  std::string Name = "_kstar_parallel_region_" + std::to_string(getId());
  std::string NumThreads = getNumThreads(numThreads_, ifClause_);
  std::string Func = wrapperFunction_ + addInjectedTemplateArgs();
  std::string Arg = structVarAddr.getaddr();

  rso << getRuntime()->EmitParallelRegion(Name, NumThreads,
                                          procBind_, Func, Arg);

  return rso.str();
}
