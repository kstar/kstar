#include "RuntimePragma.h"

using namespace clang;

RuntimeFor::RuntimeFor(OMPForDirective *S, ASTContext &C, DataSharingInfo &DSI)
    : RuntimeLoop(S, C, DSI) {}

std::string RuntimeFor::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << RuntimeLoop::genReplacePragma(q);
  rso << wrapperFunction_ << addInjectedTemplateArgs()
      << "(0, (void *)&params);";

  return rso.str();
}
