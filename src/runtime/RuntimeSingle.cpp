#include "RuntimePragma.h"
#include "GenUtils.h"
#include "Printer.h"

using namespace clang;

RuntimeSingle::RuntimeSingle(OMPSingleDirective *OMPD, ASTContext &C,
                             DataSharingInfo &DSI)
    : RuntimeScopeDirective(OMPD, C, DSI) {
}

std::string RuntimeSingle::genBeforeFunction(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << genDeclareArgStruct(argStruct_);

  if (hasInjectedTemplateArgs()) {
    rso << "template " << addInjectedTemplateArgs(true) << "\n";
  }

  // Generate callback function.
  // FIXME: Use new code generation style for this function.
  rso << "static void " << newFunction_ << "(struct " << argStruct_
      << addInjectedTemplateArgs() << " * params)"
      << "{";

  // Generate captured parameters.
  CodeGen::StructExpand argStruct = expandArgStruct("params", argStruct_);
  rso << argStruct.str();

  // Write outlined function body changing for loop iteration space
  KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective());
  p.PrintStmt(captured_->getCapturedStmt());
  rso << argStruct.free();

  rso << "}";

  // Generate copyprivate struct declaration.
  if (!copyPrivate_.empty()) {
    rso << "struct copy_private_struct" << std::to_string(getId()) << "{";
    for (auto VD : copyPrivate_)
      rso << GenUtils::asParamDownType(VD, dsi, getOMPDirective(), getContext(), true)
          << ";";
    rso << "};";
  }

  return rso.str();
}

/// \brief Replace SINGLE construct
///
/// #pragma omp single
///     block;
///
/// is replaced by:
///
/// if (single_cond() == TRUE)
///     block;
/// barrier();
///
/// and
///
/// #pragma omp single copyprivate(x)
///
/// is replaced by:
///
/// datap = single_copy_start();
/// if (datap == NULL) {
///     block;
///     data.x = x;
///     single_copy_end();
/// } else {
///     x = datap->x;
/// }
/// barrier();
std::string RuntimeSingle::genReplacePragma(std::queue<std::string> &) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string struct_var = "__kstar__single__struct__";
  CodeGen::StructInit structVarAddr =
      genFillArgStruct(struct_var, argStruct_);
  std::string functionCall = newFunction_ + addInjectedTemplateArgs() + "(" +
                             structVarAddr.getaddr() + ");";

  if (copyPrivate_.empty()) {
    rso << "if (" << getRuntime()->EmitSingleCond() << ")"
        << "{" << structVarAddr.str() << functionCall << "}";

    if (!nowait_) {
      // Add an implicit barrier when the nowait clause is FALSE.
      rso << getRuntime()->EmitBarrier();
    }
  } else {
    // The copyprivate clause has been defined for this single pragma.
    std::string copyPrivateStructDecl =
        "struct copy_private_struct" + std::to_string(getId());
    std::string copyPrivateStructName = "cpps_" + std::to_string(getId());

    rso << copyPrivateStructDecl << " " << copyPrivateStructName << ";"
        << "void *data = "
        << getRuntime()->EmitBeginCopyprivate("&" + copyPrivateStructName)
        << ";"
        << "if (data == 0)"
        << "{" << structVarAddr.str() << functionCall;

    for (auto VD : copyPrivate_) {
      std::string cast =
          GenUtils::asCastDownType(VD, dsi, getOMPDirective(), getContext());
      rso << copyPrivateStructName << "." << VD->getName() << " = " << cast
          << "&" << VD->getName() << ";";
    }
    rso << getRuntime()->EmitEndCopyprivate("&" + copyPrivateStructName) << ";";

    rso << "}"
        << "else"
        << "{";
    for (auto VD : copyPrivate_) {
      if (VD->getType()->isArrayType()) {
        if (getContext().getLangOpts().CPlusPlus) {
          rso << "for (int _i_ = 0; _i_ <  sizeof(" << VD->getName()
              << ") / sizeof(" << VD->getName() << "[0]); _i_++)\n";
          rso << VD->getName() << "[_i_] = (*((" << copyPrivateStructDecl
              << "*)data)->" << VD->getName() << ")[_i_];";
        } else {
          rso << "memcpy(" << VD->getName() << ", ((" << copyPrivateStructDecl
              << "*)data)->" << VD->getName() << ", sizeof(" << VD->getName()
              << "));";
        }
      } else {
        rso << VD->getName() << " = *((" << copyPrivateStructDecl << "*)data)->"
            << VD->getName() << ";";
      }
    }
    rso << "}" << getRuntime()->EmitCopyprivateBarrier() << ";";
  }

  return rso.str();
}
