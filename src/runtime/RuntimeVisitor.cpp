#include <clang/Basic/SourceManager.h>
#include <clang/AST/Attr.h>
#include <clang/AST/AttrIterator.h>

#include "GenUtils.h"
#include "RuntimeVisitor.h"
#include "RuntimeDirective.h"
#include "analyses/FullSourceRange.h"
#include "codegen/Util.h"
#include "Kaapi.h"

using namespace clang;

RuntimeVisitor::RuntimeVisitor(Rewriter &R, ASTContext &C)
    : Rewrite(R), Context(C), beforeFunction_(""),
      afterFunction_(""), pragmaDepth_(0) {}

bool RuntimeVisitor::VisitOMPThreadPrivateDecl(OMPThreadPrivateDecl *OMPD) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  for (auto it = OMPD->varlist_begin(); it != OMPD->varlist_end(); ++it) {
    assert(isa<DeclRefExpr>(*it) && "Unknown value in threadprivate");
    VarDecl *VD = dyn_cast_or_null<VarDecl>(cast<DeclRefExpr>(*it)->getDecl());

    ThreadStorageClassSpecifier TSCS;
    if (getContext().getLangOpts().CPlusPlus) {
      TSCS = TSCS_thread_local;
    } else {
      TSCS = TSCS__Thread_local;
    }
    VD->setTSCSpec(TSCS);

    if (VD->getType()->isRecordType()) {
      // In case this variable declaration is declared inline its record.
      const RecordDecl *RD = VD->getType()->getAsStructureType()->getDecl();
      if (RD->isEmbeddedInDeclarator()) {
        // In case of 'struct A { ...} a = { .. };' we need to print the
        // whole record declaration because it will be removed as well.
        RD->print(rso);
        rso << ";\n";
      }
    }

    // Print out the new variable with its thread storage specifier.
    VD->print(rso);
    rso << ";\n";

    // In case this line of declarations contained other variables which
    // are not referenced in any threadprivate pragmas, we need to restore
    // their declarations.
    SourceManager &SM = getRewriter().getSourceMgr();
    SourceRange SR = VD->getSourceRange();
    for (auto &GlobalVar : GlobalVariables) {
      if (VD == GlobalVar ||
          GenUtils::IsThreadPrivateVarDecl(OMPD, GlobalVar)) {
        // Do not restore a declaration when the variable is thread private.
        continue;
      }

      if (SM.getExpansionLineNumber(VD->getLocation()) ==
          SM.getExpansionLineNumber(GlobalVar->getLocation()) ||
          SM.isInSLocAddrSpace(GlobalVar->getLocation(), SR.getBegin(),
                               getRewriter().getRangeSize(SR) + 1)) {
        GlobalVar->print(rso);
        rso << ";\n";
      }
    }

    // Find next semicolon in order to remove the whole line of declarations.
    int offset = 1;
    while (true) {
      SourceLocation L = SR.getEnd().getLocWithOffset(offset);
      std::string tok = std::string(SM.getCharacterData(L), 1);
      if (tok == ";") {
        SR.setEnd(L);
        break;
      }
      offset++;
    }

    // Remove the whole line of declarations.
    getRewriter().RemoveText(SR.getBegin(), getRewriter().getRangeSize(SR) + 1);

    // When the variable has a most recent declaration, we also have to
    // insert the thread storage duration keyword to it.
    VarDecl *RecentVD = dyn_cast_or_null<VarDecl>(VD->getMostRecentDecl());
    if (RecentVD && RecentVD != VD) {
      getRewriter().InsertTextBefore(RecentVD->getTypeSpecStartLoc(),
                                 CodeGen::genTSCSpec(TSCS) + " ");
    }
  }

  // Replace the threadprivate pragma with new declarations.
  SourceRange SR = FullSourceRange(OMPD, &getRewriter()).result;
  getRewriter().ReplaceText(SR.getBegin(), getRewriter().getRangeSize(SR) + 1,
                            rso.str());
  return true;
}

bool RuntimeVisitor::VisitVarDecl(VarDecl *VD) {
  SourceManager &SM = getContext().getSourceManager();

  if (!SM.isInMainFile(VD->getLocation()))
    return true;

  if (!VD->hasGlobalStorage())
    return true;

  // Store captured global variables to help in handling threadprivate pragmas.
  GlobalVariables.insert(GlobalVariables.begin(), VD);

  return true;
}

bool RuntimeVisitor::VisitFunctionDecl(FunctionDecl *FD) {
  SourceManager &SM = getContext().getSourceManager();
  Rewriter &R = getRewriter();

  if (!SM.isInMainFile(FD->getLocation()))
    return true;

  if (!FD->hasAttr<OMPDeclareTargetDeclAttr>())
    return true;

  DeclarationNameInfo DNI = FD->getNameInfo();
  std::string signature = R.getRewrittenText(FD->getSourceRange());
  int offset = R.getRangeSize(SourceRange(FD->getSourceRange().getBegin(),
                                          DNI.getBeginLoc()));
  int len = R.getRangeSize(DNI.getSourceRange());

  std::string decls = signature + ";\n";
  decls += std::string(signature).replace(offset - len, len,
    "_gpu_" + R.getRewrittenText(DNI.getSourceRange())) +
    " __attribute__((weak));\n";
  decls += std::string(signature).replace(offset - len, len,
    "_cpu_" + R.getRewrittenText(DNI.getSourceRange())) +
    " __attribute__((weak));\n";

  // Replace 'omp target decl <declarations> omp end target decl'
  // XXX: Do not hardcode offsets!
  SourceRange SR = FullSourceRange(FD, &R).result;
  SR = SourceRange(SR.getBegin().getLocWithOffset(-27),
                   SR.getEnd().getLocWithOffset(30));
  GenUtils::ReplaceSrcRange(&R, SR, decls);

  return true;
}

bool RuntimeVisitor::isInUnusedTemplateDecl(OMPExecutableDirective *S) {
  if (const ClassTemplateDecl *CTD =
      GenUtils::getTopClassTemplateDecl(S, Context)) {
    if (CTD->spec_begin() == CTD->spec_end())
      return true;

    const CXXMethodDecl *MD = GenUtils::getTopCXXMethodDecl(S, Context);
    for (auto *S : CTD->specializations()) {
      for (auto *M : S->methods()) {
        if (M->getLocation() == MD->getLocation() && !M->isUsed())
          return true;
      }
    }
  } else if (const FunctionTemplateDecl *FTD =
             GenUtils::getTopFunctionTemplateDecl(S, Context)) {
    if (FTD->spec_begin() == FTD->spec_end())
      return true;
  }
  return false;
}

void
RuntimeVisitor::ProcessWrapOMPExecutableDirective(OMPExecutableDirective *S) {
  if (isInUnusedTemplateDecl(S)) {
    // Do no try to replace a pragma which is located inside an unused
    // template declaration, because Clang doesn't capture variables
    // when a template is not specialized. Instead remove it, and
    // add an assert call in case the code is reachable, but it should
    // not for any reasons...
    SourceRange FSR = FullSourceRange(S, &getRewriter()).result;
    SourceRange SR = SourceRange(FSR.getBegin(), S->getSourceRange().getEnd());
    std::string Pragma = GenUtils::SourceRangeToString(SR, getContext());
    std::string Code = "assert(false && \"" + Pragma + "\");\n";
    GenUtils::ReplaceSrcRange(&getRewriter(), SR, Code);
    return;
  }
  if (visited_.count(S) == 0) {
    // PRE VISIT
    pragmaDepth_++;
    // Note : deleted at endOfTranslationUnit
    if (isa<OMPCriticalDirective>(S))
      mutexNames_.insert(""); // FIXME name of critical should be set
    // FIXME Come on, there must be a better way of doing this !
    if (dsi_->hasReduce(S))
      mutexNames_.insert("reduction");
    // VISIT
    for (Stmt *Child : S->children())
      TraverseStmt(Child);
    // POST VISIT
    if (replacePragma_.size() < pragmaDepth_)
      replacePragma_.resize(pragmaDepth_);
    RuntimeDirective *RD = getRuntime()->createDirective(S, getContext(), *dsi_);
    beforeFunction_ += RD->genBeforeFunction(replacePragma_[pragmaDepth_]);
    afterFunction_ += RD->genAfterFunction(replacePragma_[pragmaDepth_]);
    pragmaDepth_--;
    spawnDirective(RD);
    if (pragmaDepth_ == 0) {
      // we reach the toplevel pragma, we can dump information and go to
      // the next one.
      postWrap_.push_back(make_tuple(RD, beforeFunction_, afterFunction_));
      beforeFunction_ = "";
      afterFunction_ = "";
    } else {
      // Free the runtime pragma object that is no longer used.
      delete RD;
    }
    visited_.insert(S);
  }
}

/*
 * For Local Stmt (Statement without context and captured statement), we don't
 * need to save state of nested pragma as there are no nested pragma
 */
void
RuntimeVisitor::ProcessLocalOMPExecutableDirective(OMPExecutableDirective *S) {
  if (visited_.count(S) == 0) {
    RuntimeDirective *RD = getRuntime()->createDirective(S, getContext(), *dsi_);
    if (replacePragma_.size() < pragmaDepth_ + 1)
      replacePragma_.resize(pragmaDepth_ + 1);
    spawnDirective(RD);
    delete RD;
    visited_.insert(S);
  }
}

// Private Functions

void RuntimeVisitor::spawnDirective(RuntimeDirective *RD) {
  OMPExecutableDirective *OMPD = RD->getOMPDirective();
  std::string Code;

  assert(OMPD && "Spawn directive on a NULL stmt");

  Code = "{";
  Code += RD->genReplacePragma(replacePragma_[pragmaDepth_ + 1]);
  Code += "}";

  if (pragmaDepth_ == 0) {
    SourceRange pragma = FullSourceRange(OMPD, &getRewriter()).result;
    GenUtils::ReplaceSrcRange(&getRewriter(), pragma, Code);
  } else {
    replacePragma_[pragmaDepth_].push(Code);
  }
}

void RuntimeVisitor::endOfTranslationUnit() {
  // effectively dump code before and after the function holding pragmas
  // One of the pragma is not remove as we need it to create mutex at correct
  // location
  for (auto &theDir : postWrap_) {
    Decl const *topLevelDecl = nullptr;
    OMPExecutableDirective *thePragma =
        std::get<0>(theDir)->getOMPDirective();
    /*
     *llvm::errs() << "Post wrap : \n";
     *llvm::errs() << "1 : \n";
     *llvm::errs() << get<1>(theDir);
     *llvm::errs() << "2 : \n";
     *llvm::errs() << get<2>(theDir);
     *llvm::errs() << "---------------";
     */

    // When the given OpenMP pragma is located inside a CXX method,
    // we have to insert the code generated by 'genBeforeFunction()'
    // inside the class itself in order to allow access to the
    // private members. Otherwise, we just have to insert the code
    // before the top declaration.
    if (CXXMethodDecl const *Method =
            GenUtils::getTopCXXMethodDecl(thePragma, getContext())) {
      // Special case when the CXX method is actually a function...
      if (FunctionTemplateDecl const *Function =
              GenUtils::getTopFunctionTemplateDecl(thePragma, getContext())) {
        topLevelDecl = static_cast<Decl const *>(Function);
      } else {
        topLevelDecl = static_cast<Decl const *>(Method);
      }
    } else if (FunctionTemplateDecl const *Function =
               GenUtils::getTopFunctionTemplateDecl(thePragma, getContext())) {
      topLevelDecl = static_cast<Decl const *>(Function);
    } else {
      topLevelDecl = GenUtils::getTopDecl(thePragma, getContext());
    }

    SourceLocation endLoc = topLevelDecl->getLocEnd();
    SourceLocation topLocBeg = topLevelDecl->getLocStart();

    getRewriter().InsertTextBefore(topLocBeg, std::get<1>(theDir));
    getRewriter().InsertTextAfterToken(endLoc, std::get<2>(theDir));
  }

  // TODO: get rid of this!
  if (getRuntime()->isKaapi()) {
    Kaapi *K = static_cast<Kaapi *>(getRuntime());
    if (not postWrap_.empty()) {
      // Create mutex with correct scope
      RuntimeDirective *dir = std::get<0>(postWrap_[0]);
      OMPExecutableDirective *thePragma = dir->getOMPDirective();
      const Decl *topLevelDecl = GenUtils::getTopDecl(thePragma, getContext());
      SourceLocation topLocBeg = topLevelDecl->getLocStart();

      std::string declMut("");
      for (std::string const &name : mutexNames_) {
        if (name == "")
          declMut += "extern ";
        declMut += K->EmitDeclMutex(name) + ";\n";
      }
      getRewriter().InsertTextBefore(topLocBeg, declMut);
    }

    // create the __attribute(contructor) and __attribute(destructor)
    // functions
    FileID file = getRewriter().getSourceMgr().getMainFileID();
    std::string txt = K->EmitConstructor(getContext(), mutexNames_);
    txt += K->EmitDestructor(getContext(), mutexNames_);
    getRewriter().InsertTextAfter(
      getRewriter().getSourceMgr().getLocForEndOfFile(file), txt);
  }

  // Free previously allocated RuntimeDirective objects.
  for (auto & it : postWrap_)
    delete std::get<0>(it);
  postWrap_.clear();
}

/*
 * Runtime's consumer main function, makes sure
 * the AST is fully traversed and set OMP Graph value and DataSharing
 * informations
 */
void RuntimeConsumer::HandleTranslationUnit(ASTContext &Context) {
  Decl *b = Context.getTranslationUnitDecl();
  DataSharingInfo dsi(b, Context);
  visitor_.setDataSharing(&dsi);
  visitor_.TraverseDecl(b);
  visitor_.endOfTranslationUnit();
}
