#include <llvm/Support/raw_ostream.h>

#include "RuntimeScopeDirective.h"
#include "GenUtils.h"
#include "Printer.h"
#include "RuntimePragma.h"

#include "codegen/StructInit.h"
#include "codegen/StructExpand.h"
#include "codegen/Util.h"
#include "context.h"

#undef KSTAR_DBG

using namespace clang;

#define ACT_ON_CLAUSE(n)                              \
  if (isa<OMP##n##Clause>(I))                         \
    ActOnOpenMP##n##Clause(cast<OMP##n##Clause>(I));

RuntimeScopeDirective::RuntimeScopeDirective(OMPExecutableDirective *oed,
                                             ASTContext &c,
                                             DataSharingInfo &dsi)
    : RuntimeDirective(oed, c), dsi(dsi),
      captured_(cast<CapturedStmt>(getOMPDirective()->getAssociatedStmt())),
      newFunction_("_kstar_callback_" + std::to_string(getId())),
      argStruct_("_kstar_struct_" + std::to_string(getId())),
      templateArgs_(ArrayRef<TemplateArgument>()),
      nowait_(false),
      numThreads_(nullptr),
      ifClause_(nullptr),
      finalCond_(nullptr),
      mergeCond_(false),
      procBind_(OMPC_PROC_BIND_unknown),
      copyIn_(nullptr),
      schedule_(OMPC_SCHEDULE_static),
      chunkSize_(nullptr),
      ordered_(false),
      untiedCond_(false),
      priority_(nullptr),
      name_(nullptr) {
  assert(oed && "Directive have to be apply on an OMPExecutableDirective");

  // Make sure to execute on CPU if the where clause is not set.
  where_ = OMPC_WHERE_cpu;

  for (OMPClause *I : getOMPDirective()->clauses()) {
    ACT_ON_CLAUSE(Nowait);
    ACT_ON_CLAUSE(NumThreads);
    ACT_ON_CLAUSE(If);
    ACT_ON_CLAUSE(ProcBind);
    ACT_ON_CLAUSE(Where);
    ACT_ON_CLAUSE(Copyin);
    ACT_ON_CLAUSE(Copyprivate);
    ACT_ON_CLAUSE(Ordered);
    ACT_ON_CLAUSE(Schedule);
    ACT_ON_CLAUSE(Final);
    ACT_ON_CLAUSE(Mergeable);
    ACT_ON_CLAUSE(Untied);
    ACT_ON_CLAUSE(Priority);
    ACT_ON_CLAUSE(Map);
    ACT_ON_CLAUSE(TaskName);
    ACT_ON_CLAUSE(Depend);
  }

  FunctionTemplateDecl *FTD = GenUtils::getTopFunctionTemplateDecl(oed, c);
  if (FTD) {
    // store injected template arguments for the top declaration
    // of this directive.
    templateArgs_ = FTD->getInjectedTemplateArgs();
  }
}

FunctionTemplateDecl *
RuntimeScopeDirective::CreateFunctionTemplate(FunctionDecl *FD) const {
  if (!hasInjectedTemplateArgs()) {
    // No template parameters are defined for this directive.
    return nullptr;
  }

  // Get template parameters.
  FunctionTemplateDecl *FTD =
    GenUtils::getTopFunctionTemplateDecl(getOMPDirective(), getContext());
  TemplateParameterList *TPL = FTD->getTemplateParameters();

  // Create template function declaration.
  const IdentifierInfo *FnId = FD->getLiteralIdentifier();
  FTD = FunctionTemplateDecl::Create(getContext(),
                                     getContext().getTranslationUnitDecl(),
                                     SourceLocation(), DeclarationName(FnId),
                                     TPL, FD);
  FD->setDescribedFunctionTemplate(FTD);

  return FTD;
}

std::string RuntimeScopeDirective::genDeclareExtraArgStruct() { return ""; }

std::string
RuntimeScopeDirective::genDeclareArgStruct(std::string const &argStructType) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string extra_def = genDeclareExtraArgStruct();

  if (hasInjectedTemplateArgs()) {
    rso << "template " << addInjectedTemplateArgs(true) << "\n";
  }

  if (!dsi.emptyOrOnlyPrivate(getOMPDirective())) {
    rso << "struct " << argStructType << "\n{\n";
    for (VarDecl const *VD : dsi.getCapturedVars(getOMPDirective(), true)) {
      if (not VD) {
#ifdef KSTAR_DBG
        rso << "  /* <kstar_this> */\n";
#endif
        QualType QT = GenUtils::getThisType(getOMPDirective(),
                                            getContext(), dsi);
        QT.print(rso, getContext().getPrintingPolicy(), "kstar_this");
        rso << ";\n";
      } else if (dsi.isPrivate(getOMPDirective(), VD)) {
        continue;
      } else if (GenUtils::isLocalStructDecl(VD)) {
#ifdef KSTAR_DBG
        rso << "  /* isLocalStructDecl: "<<VD->getNameAsString()<<" */\n";
#endif
        // We use the void pointer for local struct declarations
        rso << "void *" << VD->getNameAsString() << ";\n";
      } else {
        if (dsi.isLastPrivate(getOMPDirective(), VD)) {
#ifdef KSTAR_DBG
          rso << "  /* isLastPrivate: "<<VD->getNameAsString()<<" */\n";
#endif
          rso << GenUtils::asParamDownType(VD, dsi, getOMPDirective(), getContext(),
                                           true, true);
          rso << ";\n";
        }

        if ((not dsi.isLastPrivate(getOMPDirective(), VD)
              && (not VD->isStaticDataMember())) // cannot take the address of a static data member
            || dsi.isFirstPrivate(getOMPDirective(), VD)) {
#ifdef KSTAR_DBG
          rso << "  /* isLastPrivate: "<<dsi.isLastPrivate(getOMPDirective(), VD)<<", isStaticDataMember: "<<VD->isStaticDataMember()<<", isFirstPrivate: "<<dsi.isFirstPrivate(getOMPDirective(), VD)<<" - "<<VD->getNameAsString()<<" */\n";
#endif
          rso << GenUtils::asParamDownType(VD, dsi, getOMPDirective(), getContext(),
                                           true, false);
          rso << ";\n";
        }
      }
    }
    rso << extra_def << "};\n";
  } else if (extra_def != "")
    rso << "struct " << argStructType << "\n{\n" << extra_def << "\n};\n";
  else
    rso << "struct " << argStructType << ";\n";

  return rso.str();
}

CodeGen::StructInit RuntimeScopeDirective::genFillArgStruct(
    std::string const &argStructDest, std::string const &argStructType,
    std::vector<std::string> *by_ref) {
  CodeGen::StructInit newArgStruct(argStructType, argStructDest, true,
                                   addInjectedTemplateArgs());

  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective(), true)) {
#ifdef KSTAR_DBG
  std::string str_dbg;
  llvm::raw_string_ostream rso_dbg(str_dbg);
#endif

    if (by_ref)
      if ((std::find(by_ref->begin(), by_ref->end(), param->getName()) !=
           by_ref->end())) // In by_ref
        continue;
    if (not param) {
      newArgStruct.add("kstar_this",
                       dsi.isInner(getOMPDirective()) ? "this" : "kstar_this");
      continue;
    }
    if (dsi.isPrivate(getOMPDirective(), param))
      continue;
    std::string src_prefix;
#ifdef KSTAR_DBG
    rso_dbg << "/* " << param->getNameAsString()<< " - isArrayType: " << param->getType()->isArrayType() << ", isFirstPrivate: "<<dsi.isFirstPrivate(getOMPDirective(), param)<< " */ ";
    src_prefix = rso_dbg.str();
#else
    src_prefix = "";
#endif
    if (param->getType()->isArrayType() &&
             dsi.isFirstPrivate(getOMPDirective(), param)) {
      std::string src = src_prefix;
      src += GenUtils::getAddrVar(param, dsi, getOMPDirective());
      if (param->getType()->isVariableArrayType()) {
        std::string str;
        llvm::raw_string_ostream rso(str);

        // Generate alloc calls with cast conversions in order to
        // silent CXX compilers.
        rso << GenUtils::asCastDownType(param, dsi, getOMPDirective(), getContext(),
                                        true);
        rso << getRuntime()->EmitAlloc(CodeGen::GenSizeof(
                                       param->getNameAsString()));

        newArgStruct.add(param->getNameAsString(), rso.str());
        newArgStruct.add_memcopy(param->getNameAsString(), src,
                                 CodeGen::GenSizeof(param->getNameAsString()));
      } else if (param->getType()->isArrayType() &&
                 isa<OMPTaskDirective>(getOMPDirective())) {
        // Task firstprivate variables have to be copied at task creation
        newArgStruct.add_memcopy(param->getNameAsString(), src,
                                 CodeGen::GenSizeof(param->getNameAsString()));
      } else
        newArgStruct.add(param->getNameAsString(), src);
    } else {
      if (dsi.isLastPrivate(getOMPDirective(), param)) {
        std::string src = src_prefix;
        src +=
            GenUtils::asCastDownType(param, dsi, getOMPDirective(), getContext(), true);
        src += GenUtils::getVar(param, dsi, getOMPDirective(), true);
        newArgStruct.add("__kstar__fp_" + param->getNameAsString(), src);
      }
      if ((not dsi.isLastPrivate(getOMPDirective(), param)
            && (not param->isStaticDataMember())) // cannot take the address of a static data member
          || dsi.isFirstPrivate(getOMPDirective(), param)) {
        std::string src = src_prefix;
#ifdef KSTAR_DBG
        src += " /* asCastDownType:  */ ";
#endif
        src += GenUtils::asCastDownType(param, dsi, getOMPDirective(), getContext());
        src += GenUtils::getVar(param, dsi, getOMPDirective());
        newArgStruct.add(param->getNameAsString(), src);
      }
    }
  }
  return newArgStruct;
}

CodeGen::StructExpand
RuntimeScopeDirective::expandArgStruct(std::string const &argStructName,
                                       std::string const &argStructType) {
  CodeGen::StructExpand argStruct(argStructName, argStructType,
                                  getContext().getLangOpts().CPlusPlus,
                                  addInjectedTemplateArgs());
  // Add (local) struct declarations if needed.
  for (VarDecl const *D : dsi.getCapturedVars(getOMPDirective(), true)) {
    if (GenUtils::isLocalStructDecl(D)) {
      const Type *T = GenUtils::getRealElementType(D->getType());
      const RecordDecl *RD = T->getAsStructureType()->getDecl();

      const PrintingPolicy &PP = getContext().getPrintingPolicy();
      argStruct.addRecordDecl(GenUtils::RecordDeclToStr(RD, PP));
    }
  }

  for (VarDecl const *VD : dsi.getCapturedVars(getOMPDirective(), true)) {
    QualType QT;
    if (!VD) {
      QT = GenUtils::getThisType(getOMPDirective(), getContext(), dsi);
      argStruct.add("kstar_this", QT.getAsString(),
                    QT.getAsString() + " kstar_this");
      continue;
    }
    QT = VD->getType().getNonReferenceType();
    std::string type = GenUtils::asCastType(VD, dsi, getOMPDirective(), getContext());
    std::string paramType =
      GenUtils::asParamType(VD, dsi, getOMPDirective(), getContext());

    if (dsi.isFirstPrivate(getOMPDirective(), VD) ||
        dsi.isShared(getOMPDirective(), VD) ||
        dsi.isCopyIn(getOMPDirective(), VD)) {
      // XXX: Rewrite this ugly block.
      bool copy = (((dsi.isFirstPrivate(getOMPDirective(), VD) &&
                     not isa<OMPTaskDirective>(getOMPDirective())) ||
                    dsi.isCopyIn(getOMPDirective(), VD)) &&
                   (isa<OMPParallelDirective>(getOMPDirective()) ||
                    isa<OMPParallelSectionsDirective>(getOMPDirective()) ||
                    isa<OMPParallelForDirective>(getOMPDirective())));
      if (QT->isArrayType() && !dsi.isShared(getOMPDirective(), VD)) {
        std::string type_ =
            QT->getAsArrayTypeUnsafe()->getElementType().getAsString();
        std::string size;
        if (QT->isConstantArrayType())
          size = std::to_string(
              cast<ConstantArrayType>(QT)->getSize().getLimitedValue());
        if (getContext().getLangOpts().CPlusPlus)
          type_ = type_.substr(type_.find(' ') + 1);
        argStruct.addArray(VD->getName(), type_, size, paramType,
                           QT->getAsArrayTypeUnsafe()
                               ->getElementType()
                               ->isStructureOrClassType(),
                           copy, isa<VariableArrayType>(QT),
                           dsi.isCopyIn(getOMPDirective(), VD));
      } else {
        if (not VD->isStaticDataMember()) { // cannot take the address of a static data member
          argStruct.add(VD->getName(), type, paramType, "", false, copy,
              dsi.isCopyIn(getOMPDirective(), VD),
              (dsi.isFirstPrivate(getOMPDirective(), VD) &&
               isa<OMPTaskDirective>(getOMPDirective())));
        }
      }
    } else if (dsi.isPrivate(getOMPDirective(), VD) ||
               dsi.isLastPrivate(getOMPDirective(), VD)) {
      // Only private and lastprivate variables have their own instances
      // in outlined functions.
      argStruct.add(VD->getName(), type, paramType, "", true);
    } else if (dsi.isReduce(getOMPDirective(), VD)) {
      // Variables declared with the reduction clause have to be initialized
      // according to the reduction operator.
      std::string InitValue = genReductionInitializer(VD);
      argStruct.add(VD->getName(), type, paramType, InitValue);
    }
  }
  return argStruct;
}

std::string RuntimeScopeDirective::genReductionMinLimit(const VarDecl *VD)
{
  const LangOptions &LOpts = getContext().getLangOpts();
  std::string str;
  llvm::raw_string_ostream rso(str);
  QualType QT = VD->getType();

  if (LOpts.CPlusPlus) {
    rso << "std::numeric_limits<" << QT.getAsString() << ">::min()";
  } else {
    switch (cast<BuiltinType>(QT)->getKind()) {
    case BuiltinType::Bool:
      rso << "0";
      break;
    case BuiltinType::SChar:
      rso << "SCHAR_MIN";
      break;
    case BuiltinType::UChar:
      rso << "0";
      break;
    case BuiltinType::Short:
      rso << "SHRT_MIN";
      break;
    case BuiltinType::UShort:
      rso << "0";
      break;
    case BuiltinType::Int:
      rso << "INT_MIN";
      break;
    case BuiltinType::UInt:
      rso << "0";
      break;
    case BuiltinType::Long:
      rso << "LONG_MIN";
      break;
    case BuiltinType::ULong:
      rso << "0";
      break;
    case BuiltinType::LongLong:
      rso << "LLONG_MIN";
      break;
    case BuiltinType::ULongLong:
      rso << "0";
      break;
    case BuiltinType::Float:
      rso << "FLT_MIN";
      break;
    case BuiltinType::Double:
      rso << "DBL_MIN";
      break;
    case BuiltinType::LongDouble:
      rso << "LDBL_MIN";
      break;
    default:
      assert(!"Unsupported type of list item in a reduction clause!");
    }
  }

  return rso.str();
}

std::string RuntimeScopeDirective::genReductionMaxLimit(const VarDecl *VD)
{
  const LangOptions &LOpts = getContext().getLangOpts();
  std::string str;
  llvm::raw_string_ostream rso(str);
  QualType QT = VD->getType();

  if (LOpts.CPlusPlus) {
    rso << "std::numeric_limits<" << QT.getAsString() << ">::max()";
  } else {
    switch (cast<BuiltinType>(QT)->getKind()) {
    case BuiltinType::Bool:
      rso << "1";
      break;
    case BuiltinType::SChar:
      rso << "SCHAR_MAX";
      break;
    case BuiltinType::UChar:
      rso << "UCHAR_MAX";
      break;
    case BuiltinType::Short:
      rso << "SHRT_MAX";
      break;
    case BuiltinType::UShort:
      rso << "USHRT_MAX";
      break;
    case BuiltinType::Int:
      rso << "INT_MAX";
      break;
    case BuiltinType::UInt:
      rso << "UINT_MAX";
      break;
    case BuiltinType::Long:
      rso << "LONG_MAX";
      break;
    case BuiltinType::ULong:
      rso << "ULONG_MAX";
      break;
    case BuiltinType::LongLong:
      rso << "LLONG_MAX";
      break;
    case BuiltinType::ULongLong:
      rso << "ULLONG_MAX";
      break;
    case BuiltinType::Float:
      rso << "FLT_MAX";
      break;
    case BuiltinType::Double:
      rso << "DBL_MAX";
      break;
    case BuiltinType::LongDouble:
      rso << "LDBL_MAX";
      break;
    default:
      assert(!"Unsupported type of list item in a reduction clause!");
    }
  }

  return rso.str();
}

std::string
RuntimeScopeDirective::genReductionInitializer(const VarDecl *VD) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  const std::string Op = dsi.getReduceOperatorStr(getOMPDirective(), VD);

  if (!Op.compare("operator+") ||
      !Op.compare("operator-") ||
      !Op.compare("operator|") ||
      !Op.compare("operator^") ||
      !Op.compare("operator||")) {
    rso << "0";
  } else if (!Op.compare("operator*") ||
             !Op.compare("operator&&")) {
    rso << "1";
  } else if (!Op.compare("operator&")) {
    rso << "~0";
  } else if (!Op.compare("min")) {
    // Largest representable number in the reduction list item type.
    rso << genReductionMaxLimit(VD);
  } else if (!Op.compare("max")) {
    // Least representable number in the reduction list item type.
    rso << genReductionMinLimit(VD);
  } else {
    assert(!"Unknown reduction operator!");
  }

  return rso.str();
}

std::string
RuntimeScopeDirective::genReductionCombiner(std::string const &argStructName,
                                            std::string const &argStructType) {
  if (!dsi.hasReduce(getOMPDirective()))
    return std::string();

  std::string str;
  llvm::raw_string_ostream rso(str);
  rso << getRuntime()->EmitBeginCritical("reduction", getContext().getLangOpts().CPlusPlus11);
  for (VarDecl const *VD : dsi.getCapturedVars(getOMPDirective())) {
    if (!dsi.isReduce(getOMPDirective(), VD))
      continue;

    // 2.14.3.6 reduction clause
    // In and Out correspond to two identifiers that refer to storage of the
    // type of the list item. Out holds the final value of the combiner
    // operation.
    std::string In  = VD->getNameAsString();
    std::string Out = " *((struct " + argStructType + addInjectedTemplateArgs()
      + "*)" + argStructName + ")->" + VD->getNameAsString();

    const std::string Op = dsi.getReduceOperatorStr(getOMPDirective(), VD);
    if (!Op.compare("operator+") ||
        !Op.compare("operator-")) {
      rso << Out << " += " << In;
    } else if (!Op.compare("operator*")) {
      rso << Out << " *= " << In;
    } else if (!Op.compare("operator&")) {
      rso << Out << " &= " << In;
    } else if (!Op.compare("operator|")) {
      rso << Out << " |= " << In;
    } else if (!Op.compare("operator^")) {
      rso << Out << " ^= " << In;
    } else if (!Op.compare("operator&&")) {
      rso << Out << " = " << In << " && " << Out;
    } else if (!Op.compare("operator||")) {
      rso << Out << " = " << In << " || " << Out;
    } else if (!Op.compare("min")) {
      rso << Out << " = " << In << " < " << Out << " ? " << In << " : " << Out;
    } else if (!Op.compare("max")) {
      rso << Out << " = " << In << " > " << Out << " ? " << In << " : " << Out;
    } else {
      assert(!"Unknown reduction operator!");
    }
    rso << ";\n";
  }
  rso << getRuntime()->EmitEndCritical("reduction",
                                 getContext().getLangOpts().CPlusPlus11);

  // Inject limits header according to the target language.
  if (getContext().getLangOpts().CPlusPlus) {
    Acontext.IncludeFiles.insert("limits");
  } else {
    Acontext.IncludeFiles.insert("float.h");
    Acontext.IncludeFiles.insert("limits.h");
  }

  return rso.str();
}

std::string RuntimeScopeDirective::genForOutlinedFunction(
  std::string &newFunction, std::string &argStruct, int collapse,
  std::string const &body, bool with_for_decls) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  // Create function declaration.
  QualType ULLTy = getContext().UnsignedLongLongTy;
  QualType PtrVoidTy = getContext().getPointerType(getContext().VoidTy);
  QualType IntTy = getContext().IntTy;
  QualType ScheduleTy = getRuntime()->getScheduleType(getContext());

  SmallVector<QualType, 7> FnArgTypes;
  FnArgTypes.push_back(ULLTy);
  FnArgTypes.push_back(PtrVoidTy);
  FnArgTypes.push_back(ULLTy);
  FnArgTypes.push_back(ULLTy);
  FnArgTypes.push_back(ScheduleTy);
  FnArgTypes.push_back(IntTy);
  FnArgTypes.push_back(IntTy);

  QualType FnTy =
    getContext().getFunctionType(getContext().VoidTy, FnArgTypes,
                                 FunctionProtoType::ExtProtoInfo());
  TypeSourceInfo *FnTI =
    getContext().getTrivialTypeSourceInfo(FnTy, SourceLocation());

  IdentifierInfo *FnId = &getContext().Idents.get(newFunction);
  FunctionDecl *FD =
    FunctionDecl::Create(getContext(), getContext().getTranslationUnitDecl(),
                         SourceLocation(), SourceLocation(), FnId, FnTy, FnTI,
                         SC_Static, false, true, false);

  // Create function argument.
  TypeSourceInfo *ULLTI =
    getContext().getTrivialTypeSourceInfo(ULLTy, SourceLocation());
  TypeSourceInfo *PtrVoidTI =
    getContext().getTrivialTypeSourceInfo(PtrVoidTy, SourceLocation());
  TypeSourceInfo *IntTI =
    getContext().getTrivialTypeSourceInfo(IntTy, SourceLocation());
  TypeSourceInfo *ScheduleTI =
    getContext().getTrivialTypeSourceInfo(ScheduleTy, SourceLocation());

  ParmVarDecl *Arg1 =
    ParmVarDecl::Create(getContext(), FD, SourceLocation(), SourceLocation(),
                        &getContext().Idents.get("__kstar__j__"),
                        ULLTy, ULLTI, SC_None, 0);

  ParmVarDecl *Arg2 =
    ParmVarDecl::Create(getContext(), FD, SourceLocation(), SourceLocation(),
                        &getContext().Idents.get("params"),
                        PtrVoidTy, PtrVoidTI, SC_None, 0);

  ParmVarDecl *Arg3 =
    ParmVarDecl::Create(getContext(), FD, SourceLocation(), SourceLocation(),
                        &getContext().Idents.get("__kstar__chunk"),
                        ULLTy, ULLTI, SC_None, 0);

  ParmVarDecl *Arg4 =
    ParmVarDecl::Create(getContext(), FD, SourceLocation(), SourceLocation(),
                        &getContext().Idents.get("__kstar__pargrain__"),
                        ULLTy, ULLTI, SC_None, 0);

  ParmVarDecl *Arg5 =
    ParmVarDecl::Create(getContext(), FD, SourceLocation(), SourceLocation(),
                        &getContext().Idents.get("__kstar__schedule"),
                        ScheduleTy, ScheduleTI, SC_None, 0);
  ParmVarDecl *Arg6 =
    ParmVarDecl::Create(getContext(), FD, SourceLocation(), SourceLocation(),
                        &getContext().Idents.get("__kstar__ordered"),
                        IntTy, IntTI, SC_None, 0);

  ParmVarDecl *Arg7 =
    ParmVarDecl::Create(getContext(), FD, SourceLocation(), SourceLocation(),
                        &getContext().Idents.get("__kstar__nowait"),
                        IntTy, IntTI, SC_None, 0);

  // Set function arguments.
  ParmVarDecl *Params[] = {Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7};
  FD->setParams(Params);

  // Create and print a template function declaration if needed.
  FunctionTemplateDecl *FTD = CreateFunctionTemplate(FD);
  if (FTD) {
    FTD->print(rso);
  } else {
    FD->print(rso);
  }

  rso << "\n{\n";

  // TODO: Set body using a Stmt object instead.
  CodeGen::StructExpand expander = expandArgStruct("params", argStruct);
  rso << expander.str();
  if (with_for_decls)
    rso << GenUtils::getForDecls(captured_->getCapturedStmt(), getContext(),
                                 collapse) << "\n";
  rso << getRuntime()->EmitBarrier() << ";\n";
  rso << "unsigned long long __kstar__begin__;\n";
  rso << "unsigned long long __kstar__len__ = __kstar__j__;\n";
  rso << getRuntime()->EmitForInit("__kstar__begin__", "__kstar__j__",
                                "__kstar__chunk", "__kstar__pargrain__",
                                "__kstar__schedule",
                                "__kstar__ordered") << "\n"
      << "{\n do \n{\n";
  rso << body;
  rso << "\n}\n while ("
      << getRuntime()->EmitForNext("__kstar__len__", "__kstar__chunk",
                                "__kstar__schedule", "__kstar__ordered",
                                "__kstar__begin__", "__kstar__j__") << ");\n";
  rso << "\n}\n";

  rso << genReductionCombiner("params", argStruct) << "\n";
  rso << getRuntime()->EmitForEnd("__kstar__nowait") << "\n";
  rso << expander.free();

  rso << "\n}\n"; // end of function body

  return rso.str();
}

std::string RuntimeScopeDirective::getNumThreads(Expr *numThreads,
                                                 Expr *ifClause) {
  std::string threads = "0";
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::queue<std::string> q;
  KStarStmtPrinter P(rso, getContext().getPrintingPolicy(), q, &dsi,
                     getOMPDirective(), true, true);
  if (numThreads) {
    P.PrintExpr(numThreads);
    threads = "(" + rso.str() + ")";
  }
  rso.str().clear();
  if (ifClause) {
    P.PrintExpr(ifClause);
    threads = "(" + rso.str() + ") ? " + threads + " : 1";
  }
  return threads;
}

void RuntimeScopeDirective::ActOnOpenMPNowaitClause(OMPNowaitClause *C)
{
  nowait_ = true;
}

void RuntimeScopeDirective::ActOnOpenMPNumThreadsClause(OMPNumThreadsClause *C)
{
  numThreads_ = C->getNumThreads();
}

void RuntimeScopeDirective::ActOnOpenMPIfClause(OMPIfClause *C)
{
  ifClause_ = C->getCondition();
}

void RuntimeScopeDirective::ActOnOpenMPProcBindClause(OMPProcBindClause *C)
{
  procBind_ = C->getProcBindKind();
}

void RuntimeScopeDirective::ActOnOpenMPWhereClause(OMPWhereClause *C)
{
  where_ = C->getWhereKind();
}

void RuntimeScopeDirective::ActOnOpenMPCopyinClause(OMPCopyinClause *C)
{
  copyIn_ = C;
}

void RuntimeScopeDirective::ActOnOpenMPCopyprivateClause(OMPCopyprivateClause *C)
{
  for (auto RefExpr : C->varlists()) {
    auto *VD = cast<VarDecl>(cast<DeclRefExpr>(RefExpr)->getDecl());
    if (VD)
      copyPrivate_.push_back(VD);
  }
}

void RuntimeScopeDirective::ActOnOpenMPOrderedClause(OMPOrderedClause *C)
{
  ordered_ = true;
}

void RuntimeScopeDirective::ActOnOpenMPScheduleClause(OMPScheduleClause *C)
{
  schedule_ = C->getScheduleKind();
  chunkSize_ = C->getChunkSize();
}

void RuntimeScopeDirective::ActOnOpenMPFinalClause(OMPFinalClause *C)
{
  finalCond_ = C->getCondition();
}

void RuntimeScopeDirective::ActOnOpenMPMergeableClause(OMPMergeableClause *C)
{
  mergeCond_ = true;
}

void RuntimeScopeDirective::ActOnOpenMPUntiedClause(OMPUntiedClause *C)
{
  untiedCond_ = true;
}

void RuntimeScopeDirective::ActOnOpenMPPriorityClause(OMPPriorityClause *C)
{
  priority_ = C->getPriority();
}

void RuntimeScopeDirective::ActOnOpenMPMapClause(OMPMapClause *C)
{
  for (auto RefExpr : C->varlists()) {
    const VarDecl *VD = GenUtils::getVarDeclFromExpr(RefExpr);
    if (!VD)
      continue;

    switch (C->getMapType()) {
    case OMPC_MAP_from:
      from_[VD].insert(RefExpr);
      break;
    case OMPC_MAP_to:
      to_[VD].insert(RefExpr);
      break;
    case OMPC_MAP_alloc:
      alloc_[VD].insert(RefExpr);
      break;
    case OMPC_MAP_tofrom:
      toFrom_[VD].insert(RefExpr);
      break;
    default:
      assert(!"Invalid OpenMP map clause kind!");
      break;
    }
  }
}

void RuntimeScopeDirective::ActOnOpenMPTaskNameClause(OMPTaskNameClause *C)
{
  name_ = C->getName();
}

void RuntimeScopeDirective::ActOnOpenMPDependClause(OMPDependClause *C)
{
  for (auto RefExpr : C->varlists()) {
    const VarDecl *VD = GenUtils::getVarDeclFromExpr(RefExpr);
    if (!VD)
      continue;

    switch (C->getDependencyKind()) {
    case OMPC_DEPEND_in:
      inDeps_[VD].insert(RefExpr);
      break;
    case OMPC_DEPEND_out:
    case OMPC_DEPEND_inout:
      outDeps_[VD].insert(RefExpr);
      break;
    case OMPC_DEPEND_cw:
      cwDeps_[VD].insert(RefExpr);
      break;
    case OMPC_DEPEND_commute:
      commuteDeps_[VD].insert(RefExpr);
      break;
    default:
      assert(!"Invalid OpenMP depend clause kind!");
    }
  }
}
