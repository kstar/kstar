#include "RuntimePragma.h"
#include "GenUtils.h"
#include "Printer.h"

using namespace clang;

RuntimeLoop::RuntimeLoop(OMPExecutableDirective *S, ASTContext &C,
                         DataSharingInfo &DSI)
  : RuntimeScopeDirective(S, C, DSI),
    collapse_(GenUtils::getCollapsedNumber(S)) {
  wrapperStructType_ = "wrapper_argstruct_" + std::to_string(getId());
  wrapperFunction_ = newFunction_ + "_wrapper";
}

std::string RuntimeLoop::genBeforeFunction(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << genDeclareArgStruct(argStruct_);

  // Write outlined function declaration.
  rso << genForOutlinedFunction(newFunction_, argStruct_, collapse_,
                                genForBody(q, argStruct_));

  if (hasInjectedTemplateArgs()) {
    rso << "template " << addInjectedTemplateArgs(true) << "\n";
  }
  rso << "struct " << wrapperStructType_ << " {";
  rso << "unsigned long long end;";
  rso << "struct " << argStruct_ << addInjectedTemplateArgs() << " *values;";
  if (chunkSize_ && !chunkSize_->isIntegerConstantExpr(getContext())) {
    rso << "int chunk;";
  }
  rso << "};";
  rso << getRuntime()->genTaskSignature(getContext(), wrapperFunction_,
                                      addInjectedTemplateArgs(true));
  rso << "{\n";

  std::string chunk = "0";
  std::string pargrain = "0";
  CodeGen::StructExpand argStruct;

  std::string wrapperStructType =
      wrapperStructType_ + addInjectedTemplateArgs();

  if (chunkSize_) {
    llvm::APSInt Result;

#if 0
    // This is buggy since the rebase on top of Clang 3.9, commenting this
    // also fixes compilation of ScalFMM as a side effect.
    if (isa<DeclRefExpr>(chunkSize_->IgnoreImplicit())) {
      // When the chunksize is not a constant expression, try to get the
      // initializer expression.
      DeclRefExpr *Ref = cast<DeclRefExpr>(chunkSize_->IgnoreImplicit());
      VarDecl *VD = cast<VarDecl>(Ref->getDecl());

      if (VD->hasInit()) {
        // Get the variable declaration.
        DeclRefExpr *InitRef =
          cast<DeclRefExpr>(VD->getInit()->IgnoreImplicit());
        VarDecl *InitVD = cast<VarDecl>(InitRef->getDecl());

        if (InitVD->hasInit()) {
          // Replace the chunksize expression and hope that it's an
          // IntegerLiteral expression.
          chunkSize_ = InitVD->getInit();
        }
      }
    }
#endif

    if (chunkSize_ && !chunkSize_->EvaluateAsInt(Result, getContext())) {
      argStruct = expandArgStruct(
        "((struct " + wrapperStructType_ + "*)kstar_params)->values", argStruct_);
      rso << argStruct.str();
    }

    // Evaluate the chunk_size clause as an integer.
    if (chunkSize_->EvaluateAsInt(Result, getContext())) {
      chunk = Result.toString(10);
    } else {
      chunk = "((struct " +  wrapperStructType +  "*)kstar_params)->chunk";
    }
  }

  rso << newFunction_ << addInjectedTemplateArgs() << "(((struct "
      << wrapperStructType << "*)kstar_params)->end,"
      << "((struct " << wrapperStructType << "*)kstar_params)->values," << chunk
      << ", " << pargrain << ", " << getRuntime()->EmitScheduleType(schedule_)
      << ", " << ordered_ << ", 0);\n";

  if (chunkSize_)
    rso << argStruct.free();

  rso << "}\n";
  return rso.str();
}

std::string RuntimeLoop::genReplacePragma(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  // Generate end condition of the loop.
  rso << "(unsigned long long)(1";
  for (int i = 1; i <= collapse_; i++) {
    ForStmt *For = getForStmt(captured_->getCapturedStmt(), i);
    if (For)
      rso << " * " + genUpperBound(For, true);
  }
  rso << ")";

  // Generate argstruct filling.
  std::string structVar("_kstar_ctx_" + std::to_string(getId()));
  CodeGen::StructInit structVarAddr =
      genFillArgStruct(structVar, argStruct_);

  CodeGen::StructInit wrapper(wrapperStructType_ + addInjectedTemplateArgs(),
                              "params", true);
  wrapper.add("end", rso.str());
  wrapper.add("values", structVarAddr.getaddr());
  if (chunkSize_ && !chunkSize_->isIntegerConstantExpr(getContext())) {
    std::string chunk = "0";
    std::string dummy;
    llvm::raw_string_ostream dummys(dummy);
    KStarStmtPrinter p(dummys, getContext().getPrintingPolicy(), q, &dsi,
                       getOMPDirective());
    const VarDecl *VD = GenUtils::getVarDeclFromExpr(chunkSize_);
    if (VD->getInit())
      p.PrintExpr(const_cast<Expr *>(VD->getInit()));
    else
      p.PrintExpr(chunkSize_);
    chunk = dummys.str();
    wrapper.add("chunk", chunk);

    dummys.str().clear();
  }

  return structVarAddr.str() + wrapper.str();
}

std::string RuntimeLoop::genForBody(std::queue<std::string> &q,
                                    std::string const &argStruct) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << "for(unsigned long long __kstar__i__ = __kstar__begin__; __kstar__i__ "
         "< __kstar__j__; __kstar__i__++){";
  for (int i = 1; i <= collapse_; i++) {
    ForStmt *For = getForStmt(captured_->getCapturedStmt(), i);
    if (!For)
      break;

    rso << getIterationVariable(For)->getName() << " = " << genBegin(For);
    if (isIncr(For)) {
      rso << " + ";
    } else {
      rso << " - ";
    }

    rso << "(((__kstar__i__ / ( 1";
    for (int j = i + 1; j <= collapse_; j++) {
      ForStmt *For = getForStmt(captured_->getCapturedStmt(), j);
      if (!For)
        break;
      rso << "* " << genUpperBoundNoCheck(For);
    }
    rso << ")) % " << genUpperBoundNoCheck(For)
        << ") * (" << genStep(For) << "));";
  }

  // Write outlined function body.
  Stmt *capStmt = captured_->getCapturedStmt();
  ForStmt *curFor = nullptr;
  std::string lastIter = "";
  for (int i = 0; i < collapse_; i++) {
    while (isa<CompoundStmt>(capStmt))
      capStmt = *cast<CompoundStmt>(capStmt)->body_begin();
    assert(isa<ForStmt>(capStmt) && "OMP For should contain ForStmt");
    curFor = cast<ForStmt>(capStmt);
    capStmt = curFor->getBody();
    std::string dummy;
    llvm::raw_string_ostream dummys(dummy);
    KStarStmtPrinter p(dummys, getContext().getPrintingPolicy(), q, &dsi,
                       getOMPDirective());
    p.PrintStmt(curFor->getInc());
    lastIter += dummys.str();
  }
  KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi, getOMPDirective());
  p.PrintStmt(capStmt);
  rso << "}\n";
  rso << lastIter;

  // Generate code for lastprivate variables.
  if (dsi.hasLastPrivate(getOMPDirective())) {
    rso << "if(__kstar__j__ == (1";
    for (int i = 1; i <= collapse_; i++) {
      ForStmt *For = getForStmt(captured_->getCapturedStmt(), i);
      rso << "* " << genUpperBoundNoCheck(For);
    }
    rso << ")) {\n";
    for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
      if (dsi.isLastPrivate(getOMPDirective(), param)) {
        rso << "* ((struct " << argStruct << addInjectedTemplateArgs()
            << "* )params)->__kstar__fp_" << param->getName() << " = "
            << param->getName() << ";\n";
      }
    }
    rso << "}\n";
  }
  return rso.str();
}

std::string RuntimeLoop::genUpperBound(ForStmt *For, bool parent) const {
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string Begin = genBegin(For, parent, true);
  std::string End = genEnd(For, parent, true);

  if (isIncr(For)) {
    rso << "((" << End << " >= " << Begin << ") ? ";
  } else {
    rso << "((" << Begin << " >= " << End << ") ? ";
  }
  rso << genUpperBoundNoCheck(For, parent, true) << " : 0)";
  return rso.str();
}

std::string RuntimeLoop::genUpperBoundNoCheck(ForStmt *For, bool parent,
                                              bool is_outer) const {
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string Step = genStep(For, parent);
  std::string Begin = genBegin(For, parent, is_outer);
  std::string End = genEnd(For, parent, is_outer);

  if (isIncr(For)) {
    rso << "(((" << End << ") - (" << Begin << ") + (" << Step << ") - 1) / ("
        << Step << "))";
  } else {
    rso << "(((" << Begin << ") - (" << End << ") + (" << Step << ") - 1) / ("
        << Step << "))";
  }
  return rso.str();
}

bool RuntimeLoop::isIncr(ForStmt *For) const {
  BinaryOperator *Op = cast<BinaryOperator>(For->getCond());
  Expr *LHS = Op->getLHS()->IgnoreImpCasts();
  const VarDecl *VD = GenUtils::getVarDeclFromExpr(LHS);

  if (VD == getIterationVariable(For)) {
    // If test-expr is of the form var relational-op b and relational-op
    // is < or <= then incr-expr must cause var to increase on each iteration
    // of the loop. If test-expr is of the form var relational-op b and
    // relational-op is > or >= then incr-expr must cause var to decrease on
    // each iteration of the loop.
    if (Op->getOpcode() == BO_LT || Op->getOpcode() == BO_LE)
      return true;
  } else {
    // If test-expr is of the form b relational-op var and relational-op
    // is < or <= then incr-expr must cause var to decrease on each iteration
    // of the loop. If test-expr is of the form b relational-op var and
    // relational-op is > or >= then incr-expr must cause var to increase on
    // each iteration of the loop.
    if (Op->getOpcode() == BO_GT || Op->getOpcode() == BO_GE)
      return true;
  }
  return false;
}

std::string RuntimeLoop::genStep(ForStmt *For, bool parent) const {
  Expr *Inc = For->getInc();
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::queue<std::string> q;
  KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi,
                     getOMPDirective(), parent);

  if (isa<UnaryOperator>(Inc)) {
    // var++, ++var, var--, --var
    return "1";
  } else {
    BinaryOperator *Op = cast<BinaryOperator>(Inc);
    if (Op->getOpcode() == BO_AddAssign || Op->getOpcode() == BO_SubAssign) {
      // var += incr, var -= incr
      p.PrintExpr(Op->getRHS());
      // XXX: WTF?
      std::string &step = rso.str();
      return (step[0] == '-') ? step.substr(1) : step;
    } else {
      // var = var + incr, var = incr + var, var = var - incr
      Op = cast<BinaryOperator>(Op->getRHS());
      Expr *LHS = Op->getLHS()->IgnoreImpCasts();
      const VarDecl *VD = GenUtils::getVarDeclFromExpr(LHS);
      if (VD == getIterationVariable(For)) {
        // var relation-op b
        p.PrintExpr(Op->getRHS());
      } else {
        // b relation-op var
        p.PrintExpr(Op->getLHS());
      }

      // XXX: WTF?
      std::string &step = rso.str();
      return (step[0] == '-') ? step.substr(1) : step;
    }
  }
  return "";
}

std::string
RuntimeLoop::genEnd(ForStmt *For, bool parent, bool is_outer) const {
  BinaryOperator *Op = cast<BinaryOperator>(For->getCond());
  Expr *LHS = Op->getLHS()->IgnoreImpCasts();
  const VarDecl *VD = GenUtils::getVarDeclFromExpr(LHS);

  std::string str;
  llvm::raw_string_ostream rso(str);
  std::queue<std::string> q;
  KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi,
                     getOMPDirective(), parent, is_outer);

  if (VD == getIterationVariable(For)) {
    // var relational-op b
    p.PrintExpr(Op->getRHS());
    if (Op->getOpcode() == BO_GE) {
      rso << " -1";
    } else if (Op->getOpcode() == BO_LE) {
      rso << " +1";
    }
  } else {
    // b relational-op var
    p.PrintExpr(Op->getLHS());
    if (Op->getOpcode() == BO_GE) {
      rso << " +1";
    } else if (Op->getOpcode() == BO_LE) {
      rso << " -1";
    }
  }
  return rso.str();
}

std::string
RuntimeLoop::genBegin(ForStmt *For, bool parent, bool is_outer) const {
  Expr *Init = getPreCond(For);
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::queue<std::string> q;
  KStarStmtPrinter p(rso, getContext().getPrintingPolicy(), q, &dsi,
                     getOMPDirective(), parent, is_outer);
  p.PrintExpr(Init);
  return rso.str();
}

ForStmt *RuntimeLoop::getForStmt(Stmt *S, int Depth) const {
  ForStmt *For = nullptr;
  for (int i = 0; i < Depth; i++) {
    while (isa<CompoundStmt>(S))
      S = *(cast<CompoundStmt>(S)->body_begin());
    For = cast<ForStmt>(S);
    S = For->getBody();
  }
  return For;
}

Expr *RuntimeLoop::getPreCond(ForStmt *For) const {
  Stmt *Init = For->getInit();
  if (isa<BinaryOperator>(Init)) {
    // var = lb
    return cast<BinaryOperator>(Init)->getRHS()->IgnoreImpCasts();
  } else  if (isa<DeclStmt>(Init)) {
    // integer-type var = lb
    DeclStmt *DS = cast<DeclStmt>(Init);
    if (isa<VarDecl>(DS->getSingleDecl()))
      return cast<VarDecl>(DS->getSingleDecl())->getInit();
  }
  return nullptr;
}

const VarDecl *RuntimeLoop::getIterationVariable(ForStmt *For) const {
  Stmt *Init = For->getInit();
  if (isa<BinaryOperator>(Init)) {
    // var = lb
    Expr *E = cast<BinaryOperator>(Init)->getLHS()->IgnoreImpCasts();
    return GenUtils::getVarDeclFromExpr(E);
  } else  if (isa<DeclStmt>(Init)) {
    // integer-type var = lb
    DeclStmt *DS = cast<DeclStmt>(Init);
    if (isa<VarDecl>(DS->getSingleDecl()))
      return cast<VarDecl>(DS->getSingleDecl());
  }
  return nullptr;
}
