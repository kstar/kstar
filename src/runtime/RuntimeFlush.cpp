#include "RuntimePragma.h"

using namespace clang;

RuntimeFlush::RuntimeFlush(OMPFlushDirective *t, ASTContext &c,
                           DataSharingInfo &dsa)
    : RuntimeDirective(t, c) {}

std::string RuntimeFlush::genReplacePragma(std::queue<std::string> &) {
  return getRuntime()->EmitFlush();
}
