#include "RuntimePragma.h"

using namespace clang;

RuntimeTaskwait::RuntimeTaskwait(OMPTaskwaitDirective *t, ASTContext &c,
                                 DataSharingInfo &dsi)
    : RuntimeDirective(t, c) {}

std::string RuntimeTaskwait::genReplacePragma(std::queue<std::string> &) {
  return getRuntime()->EmitTaskwait();
}
