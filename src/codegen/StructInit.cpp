#include "codegen/StructInit.h"
#include "codegen/Util.h"

namespace CodeGen {

StructInit::StructInit(const std::string &I, const std::string &N, bool inline_,
                       const std::string &T )
    : Typename(I), Name(N), Template(T), Inline(inline_) {}

void StructInit::add(const std::string &Type, const std::string &Name) {
  Members.push_back(make_pair(Type, Name));
}

void StructInit::add_memcopy(const std::string &field, const std::string &src,
                             const std::string &len) {
  Memcopy.insert(make_pair(field, make_pair(src, len)));
}

void StructInit::add_depend(const std::string &Type, const std::string &Name) {
  DependMembers.push_back(make_pair(Type, Name));
}


std::string StructInit::getaddr() const {
  if (Members.empty() && Memcopy.empty())
    return "0";
  else if (Dynalloc.empty())
    return "&" + Name;
  else
    return Name;
}

std::string StructInit::get(std::string const &attr) const {
  std::string link = Dynalloc.empty() ? "." : "->";
  return Name + link + attr;
}

void StructInit::setDynalloc(std::string const &dynalloc) {
  Dynalloc = dynalloc;
  Inline = false;
}

std::string StructInit::getType() const {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << "struct ";
  rso << Typename;
  if (!Template.empty())
    rso << Template;
  if (not Dynalloc.empty())
    rso << "*";
  return rso.str();
}

std::string StructInit::str() const {
  if (Members.empty() && Memcopy.empty())
    return "";

  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << getType() << " " << Name;
  // struct can be allocate on the stack and initilized inline.
  assert(Dynalloc.empty() or not Inline);
  std::string link = Dynalloc.empty() ? "." : "->";
  if (Inline) {
    rso << " = {";
    bool first = true;
    for (auto it = Members.begin(); it != Members.end(); ++it) {
      if (not first)
        rso << ",";
      rso << "\n";
      first = false;
      rso << it->second;
    }
    rso << "\n};\n";
  } else {
    if (not Dynalloc.empty())
      rso << " = (" << getType() << ")" << Dynalloc;
    else
      rso << " = {0}";
    rso << ";\n";
    for (auto it = Members.begin(); it != Members.end(); ++it)
      rso << Name << link << it->first << " = " << it->second << ";\n";
  }

  /* currently only for Kaapi */
  for (auto it = DependMembers.begin(); it != DependMembers.end(); ++it) {
    rso << "komp_access_init( &" << Name << link << it->first << "," << it->second << ");\n";
  }

  for (auto it = Memcopy.begin(); it != Memcopy.end(); ++it) {
    rso << CodeGen::GenMemcpy(Name + link + it->first, it->second.first,
                              it->second.second);
    rso << ";\n";
  }

  return rso.str();
}

} // end namespace CodeGen
