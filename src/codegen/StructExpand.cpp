#include "codegen/StructExpand.h"
#include "codegen/Util.h"

namespace CodeGen {

StructExpand::StructExpand(const std::string &structName,
                           const std::string &structType, bool is_cpp,
                           const std::string &templateArgs)
    : structName_(structName), structType_(structType), IsCXX(is_cpp),
      templateArgs_(templateArgs) {}

void StructExpand::add(const std::string &name, const std::string &type,
                       const std::string &paramType,
                       const std::string &default_, bool private_, bool copy,
                       bool copyin, bool ref) {
  Variables_.push_back(std::make_tuple(name, type, default_, private_, copy,
                                       paramType, copyin, ref));
}

void StructExpand::addRecordDecl(const std::string &record) {
  Records_.push_back(record);
}

void StructExpand::addArray(const std::string &name, const std::string &type,
                            const std::string &len,
                            const std::string &paramType, bool is_struct,
                            bool copy, bool isvla, bool copyin) {
  ArrayVariables_.push_back(
      std::make_tuple(name, type, len, paramType, is_struct,
                      copy, isvla, copyin));
}

void StructExpand::except(std::vector<std::string> const &to_handle) {
  {
    decltype(Variables_) tmp;
    for (auto &name : Variables_)
      if (std::find(to_handle.begin(), to_handle.end(), std::get<0>(name)) ==
          to_handle.end())
        tmp.push_back(name);
    Variables_.clear();
    std::copy(tmp.begin(), tmp.end(), back_inserter(Variables_));
  }
  {
    decltype(ArrayVariables_) tmp;
    for (auto &name : ArrayVariables_)
      if (std::find(to_handle.begin(), to_handle.end(), std::get<0>(name)) ==
          to_handle.end())
        tmp.push_back(name);
    ArrayVariables_.clear();
    std::copy(tmp.begin(), tmp.end(), back_inserter(ArrayVariables_));
  }
}

std::string StructExpand::str() {
  std::string str;
  llvm::raw_string_ostream rso(str);

  for (auto &record : Records_) {
    rso << record;
    rso << ";\n";
  }

  for (auto &variable : Variables_) {
    if (std::get<6>(variable)) { // Case of CopyIn variable
      rso << "if(omp_get_thread_num() != 0)\n"; // FIXME Only one check should
                                                // be done for all copyins
      rso << std::get<0>(
          variable); // Variable alreay exists so we just assign it
    } else
      rso << std::get<5>(variable); // Create correct variable.
    if (std::get<3>(variable)) {    // If it is private, we just want a
                                    // declaration, no definition
      rso << ";\n";
      continue;
    }
    rso << " = "; //(" << std::get<1>(variable) << ")"; // no cast as it imply
                  //an extra object creation.
    if (std::get<2>(variable) != "")
      rso << std::get<2>(variable)
          << ";\n"; // Add a default value to assign (case of reduction).
    else {
      if (std::get<4>(variable)) {
        // If we want a copy, add a dereferencement.
        rso << "*";
      } else if (std::get<7>(variable)) {
        // If we want a ref, ask for adresse
        rso << "&";
      } else {
        if (!std::get<1>(variable).empty()) {
          // Add explicit cast for C++ compilers. This fixes an invalid
          // conversion mainly related to local data structs where a
          // void pointer is used.
          rso << "(" << std::get<1>(variable) << ")";
        }
      }

      rso << "((struct " << structType_ << templateArgs_ << "*)" << structName_
          << ")->" << std::get<0>(variable)
          << ";\n"; // Assign value from structure.
    }
  }

  // Special case of first private array.
  for (auto &variable : ArrayVariables_) {
    std::string src = "((struct " + structType_ + "*)" + structName_ + ")->" +
                      std::get<0>(variable);

    if (not std::get<5>(variable)) { // If no copy is needed
      rso << std::get<3>(variable) << " = ";
      if (not std::get<6>(variable))
        rso << "&";
      rso << src << " ;\n";
    } else if (IsCXX) {
      // Allocation without initialisation
      rso << std::get<1>(variable) << "* _kstar_alloc_" << std::get<0>(variable)
          << " = "
          << "static_cast<" << std::get<1>(variable) << "*>(::operator new ("
          << CodeGen::GenSizeof(std::get<1>(variable)) << " * "
          << std::get<2>(variable) << "));\n";
      // Initialisation using copy constructor
      rso << "for(int _kstar_iter_alloc = 0; _kstar_iter_alloc < "
          << std::get<2>(variable) << "; _kstar_iter_alloc++)\n"
          << "new(&_kstar_alloc_" << std::get<0>(variable)
          << "[_kstar_iter_alloc]) " << std::get<1>(variable) << "((*((struct "
          << structType_ << "* )" << structName_ << ")->"
          << std::get<0>(variable) << ")[_kstar_iter_alloc]);\n";
      // assignement to correct variable name
      rso << "typedef " << std::get<1>(variable) << " _kstar_type"
          << std::get<0>(variable) << "[" << std::get<2>(variable) << "]"
          << ";\n";
      rso << "_kstar_type" << std::get<0>(variable) << "& "
          << std::get<0>(variable) << " = *reinterpret_cast<"
          << "_kstar_type" << std::get<0>(variable) << "*>(_kstar_alloc_"
          << std::get<0>(variable) << ");\n";
    } else {
      if (std::get<7>(variable)) { // Case of CopyIn variable
        rso << "if(omp_get_thread_num() != 0)\n";
      } else {
        rso << std::get<3>(variable) << ";\n";
      }
      std::string n = CodeGen::GenSizeof(std::get<1>(variable)) + " * " +
                      std::get<2>(variable);
      rso << CodeGen::GenMemcpy(std::get<0>(variable), src, n) << ";\n";
    }
  }
  return rso.str();
}

std::string StructExpand::free() {
  std::string str;
  llvm::raw_string_ostream rso(str);
  for (auto &variable : ArrayVariables_) {
    if (IsCXX and std::get<5>(variable)) {
      if (std::get<4>(variable)) {
        // Remove calling destructor
        rso << "for(int _kstar_iter_alloc = 0; _kstar_iter_alloc < "
            << std::get<2>(variable) << "; _kstar_iter_alloc++)\n"
            << "_kstar_alloc_" << std::get<0>(variable)
            << "[_kstar_iter_alloc].~" << std::get<1>(variable) << "();\n";
      }
      rso << "::operator delete(_kstar_alloc_" << std::get<0>(variable)
          << ");\n";
    }
  }
  return rso.str();
}
}
