#include "analyses/VariablesVisitor.h"
#include "GenUtils.h"

using namespace clang;

GlobalVariablesVisitor::GlobalVariablesVisitor(Stmt const *S) {
  TraverseStmt(const_cast<Stmt *>(S));
}

bool GlobalVariablesVisitor::VisitDeclRefExpr(DeclRefExpr const *S) {
  VarDecl const *VD = dyn_cast_or_null<VarDecl const>(S->getDecl());

  if (!VD)
    return true;

  if (VD->hasExternalStorage()) {
    // Do not add a variable which has extern
    // or __private_extern__storage.
    return true;
  }

  if (VD->hasGlobalStorage() && !VD->isStaticLocal())
    __CapturedVars.insert(VD);

  return true;
}

LocalVariablesVisitor::LocalVariablesVisitor(Stmt const *S) {
  TraverseStmt(const_cast<Stmt *>(S));
}

bool LocalVariablesVisitor::VisitDeclRefExpr(DeclRefExpr const *S) {
  VarDecl const *VD = dyn_cast_or_null<VarDecl const>(S->getDecl());

  if (VD && VD->hasLocalStorage())
    __CapturedVars.insert(VD);

  return true;
}

StaticLocalVariablesVisitor::StaticLocalVariablesVisitor(Stmt const *S) {
  TraverseStmt(const_cast<Stmt *>(S));
}

bool StaticLocalVariablesVisitor::VisitDeclRefExpr(DeclRefExpr const *S) {
  VarDecl const *VD = dyn_cast_or_null<VarDecl const>(S->getDecl());

  if (VD && VD->isStaticLocal())
    __CapturedVars.insert(VD);

  return true;
}

CapturedVariablesVisitor::CapturedVariablesVisitor(Decl *D,
                                                   OMPExecutableDirective *OMPD)
    : Directive(OMPD) {
  TraverseDecl(const_cast<Decl *>(D));
}

bool CapturedVariablesVisitor::VisitOMPExecutableDirective(
    OMPExecutableDirective *OMPD) {
  if ((OMPD->getLocStart() != Directive->getLocStart()) ||
      (OMPD->getLocEnd() != Directive->getLocEnd()))
    return true;

  __CapturedVars = GenUtils::getCapturedVars(OMPD);
  return false;
}
