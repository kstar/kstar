#include <set>
#include <vector>

#include "analyses/DSA.h"
#include "analyses/VariablesVisitor.h"
#include "GenUtils.h"

using namespace clang;

DataSharingInfo::DataSharingInfo(Decl const *D, ASTContext &C)
    : nestedOmp(D), Context(C) {
  TraverseDecl(const_cast<Decl *>(D));
}

#define DATACLAUSE(OPERATOR, TYPE, DATASHARING)                                \
  case OPERATOR: {                                                             \
    TYPE *C = cast<TYPE>(I);                                                   \
    for (TYPE::varlist_iterator II = C->varlist_begin(),                       \
                                EE = C->varlist_end();                         \
         II != EE; ++II) {                                                     \
      const VarDecl *VD = GenUtils::getVarDeclFromExpr(*II);                   \
      if (!VD)                                                                 \
        continue;                                                              \
      result[S][VD] |= DATASHARING;                                            \
    }                                                                          \
  } break;

bool DataSharingInfo::VisitFunctionDecl(FunctionDecl const *FD) {
  __CurrentFunctionDecl = FD;
  return true;
}

bool DataSharingInfo::VisitOMPThreadPrivateDecl(OMPThreadPrivateDecl *OMPD) {
  for (auto it = OMPD->varlist_begin(); it != OMPD->varlist_end(); ++it) {
    assert (isa<DeclRefExpr>(*it) && "Unknown value in threadprivate");
    VarDecl *VD = dyn_cast_or_null<VarDecl>(cast<DeclRefExpr>(*it)->getDecl());
    __ThreadPrivateVars.insert(__ThreadPrivateVars.begin(), VD);
  }
  return true;
}

void DataSharingInfo::ProcessOMPExecutableDirective(OMPExecutableDirective *S) {
  bool defaultShared = false;
  // Process explicit data sharing information.
  for (OMPClause *I : S->clauses()) {
    switch (I->getClauseKind()) {
      DATACLAUSE(OMPC_shared, OMPSharedClause, DataSharing::DS_Shared)
      DATACLAUSE(OMPC_firstprivate, OMPFirstprivateClause,
                 DataSharing::DS_FirstPrivate)
      DATACLAUSE(OMPC_lastprivate, OMPLastprivateClause,
                 DataSharing::DS_LastPrivate)
      DATACLAUSE(OMPC_private, OMPPrivateClause, DataSharing::DS_Private)
      DATACLAUSE(OMPC_copyin, OMPCopyinClause, DataSharing::DS_CopyIn)
      DATACLAUSE(OMPC_reduction, OMPReductionClause, DataSharing::DS_Reduce)
      case OMPC_default:
        {
          OMPDefaultClause *C = cast<OMPDefaultClause>(I);
          defaultShared = C->getDefaultKind() == OMPC_DEFAULT_shared;
           // We don't check for shared as it is done by backend.
        }
        break;
      case OMPC_linear:
        assert(!"Unsupported linear data-sharing clause!");
        break;
      default:
        break;
    }
  }

  CapturedStmt *captured = cast<CapturedStmt>(S->getAssociatedStmt());
  int CollapsedNum = GenUtils::getCollapsedNumber(S);
  std::vector<VarDecl *> iterDecl(CollapsedNum);
  if (isa<OMPForDirective>(S) || isa<OMPParallelForDirective>(S)) {
    Stmt *capStmt = captured->getCapturedStmt();
    for (int i = 0; i < CollapsedNum; i++) {
      while (isa<CompoundStmt>(capStmt))
        capStmt = *cast<CompoundStmt>(capStmt)->body_begin();
      iterDecl[i] = GenUtils::getIterDecl(cast<ForStmt>(capStmt));
      capStmt = cast<ForStmt>(capStmt)->getBody();
    }
  }

  // Store the function declaration where this OpenMP pragma is defined.
  __FunctionDecl[S] = __CurrentFunctionDecl;

  CaptureGlobalVariables(S);
  CaptureVariables(S);
  for (VarDecl const *VD : getCapturedVars(S)) {
    implicitDataSharing(VD, iterDecl, S, defaultShared);

    // Skip shared global threadprivate variables when the strange withDSA flag is invoked.
    if (isGlobal(S, VD) && !VD->isStaticLocal() &&
        isShared(S, VD) && !isLocal(S, VD) &&
        isThreadPrivateVar(VD))
      continue;
    __CapturedVarsDSA[S].insert(__CapturedVarsDSA[S].end(), VD);
  }
}

void DataSharingInfo::implicitDataSharing(VarDecl const *VD,
                                          std::vector<VarDecl *> iterDecl,
                                          OMPExecutableDirective *S,
                                          bool defaultShared) {
  if (!VD) {
    // No data sharing for "this" pointer.
    return;
  }

  if (result[S][VD]) {
    // Data sharing has already been defined for this variable.
    return;
  }

  // 2.14.1.1
  // The loop iteration variable(s) in the associated for-loop(s) of a for or
  // parallelfor construct is (are) private.
  if (std::find(iterDecl.begin(), iterDecl.end(), VD) != iterDecl.end())
    result[S][VD] |= DataSharing::DS_Private;
  // 2.14.1.1
  // In a parallel or task construct, the data-sharing attributes of these
  //(implicitly determined datasharing) variables are determined by the
  // default clause, if present
  else if (defaultShared)
    result[S][VD] |= DataSharing::DS_Shared;
  // 2.14.1.1
  // For constructs other than task, if no default clause is present, these
  // variables inherit their data-sharing attributes from the enclosing context
  else if ((result[nestedOmp.getFirstNode(S)][VD] & DataSharing::DS_Shared) ||
           VD->hasGlobalStorage())
    result[S][VD] |= DataSharing::DS_Shared;
  // 2.14.1.1
  // In a task construct, if no default clause is present, a variable whose
  // data-sharing attribute is not determined by the rules above is
  // firstprivate
  else if (isa<OMPTaskDirective>(S) || isa<OMPTargetDirective>(S))
    result[S][VD] |= DataSharing::DS_FirstPrivate;
  // 2.14.1.1
  // In a parallel construct, if no default clause is present, these variables
  // are shared
  else
    result[S][VD] |= DataSharing::DS_Shared;
}

bool DataSharingInfo::isReduce(OMPExecutableDirective *S, VarDecl const *VD) {
  return exists(S, VD) && (result[S][VD] & DataSharing::DS_Reduce);
}

const std::string
DataSharingInfo::getReduceOperatorStr(OMPExecutableDirective *S,
                                      VarDecl const *VD) {
  for (OMPClause *I : S->clauses()) {
    if (I->getClauseKind() == OMPC_reduction) {
      OMPReductionClause *C = cast<OMPReductionClause>(I);
      for (auto RefExpr : C->varlists()) {
        if (GenUtils::getVarDeclFromExpr(RefExpr) == VD) {
          // XXX: Probably not the most effficient way to return the reduce
          // operator as string but it works.
          return C->getNameInfo().getAsString();
        }
      }
    }
  }
  return std::string("unknown");
}

bool DataSharingInfo::isPrivate(OMPExecutableDirective *S, VarDecl const *VD) {
  return exists(S, VD) && (result[S][VD] & DataSharing::DS_Private);
}

bool DataSharingInfo::isShared(OMPExecutableDirective *S, VarDecl const *VD) {
  return exists(S, VD) && (result[S][VD] & DataSharing::DS_Shared);
}

bool DataSharingInfo::isFirstPrivate(OMPExecutableDirective *S,
                                     VarDecl const *VD) {
  return exists(S, VD) && (result[S][VD] & DataSharing::DS_FirstPrivate);
}

bool DataSharingInfo::isCopyIn(OMPExecutableDirective *S, VarDecl const *VD) {
  return exists(S, VD) && (result[S][VD] & DataSharing::DS_CopyIn);
}

bool DataSharingInfo::hasReduce(OMPExecutableDirective *S) {
  for (auto &VD : result[S])
    if (VD.second & DataSharing::DS_Reduce)
      return true;
  return false;
}

bool DataSharingInfo::hasLastPrivate(OMPExecutableDirective *S) {
  for (auto &VD : result[S])
    if (VD.second & DataSharing::DS_LastPrivate)
      return true;
  return false;
}

bool DataSharingInfo::isLastPrivate(OMPExecutableDirective *S,
                                    VarDecl const *VD) {
  return exists(S, VD) && (result[S][VD] & DataSharing::DS_LastPrivate);
}

bool DataSharingInfo::isInner(OMPExecutableDirective *S) {
  OMPExecutableDirective *Parent = nestedOmp.getFirstNode(S);
  if (isInlineDirective(S)) {
    if (!Parent) {
      // An "inline" directive is inside a class when
      // no parent exists.
      return true;
    }

    while (Parent) {
      if (!isInlineDirective(Parent)) {
        // An "inline" directive is outside a class when
        // a parent node is an "outline" directive.
        return false;
      }
      Parent = nestedOmp.getFirstNode(Parent);
    }
  }

  // An "outline" directive is outside a class when the parent node exists.
  return Parent ? false : true;
}

bool DataSharingInfo::isThreadPrivateVar(clang::VarDecl const *VD) {
   return (__ThreadPrivateVars.find(VD) != __ThreadPrivateVars.end());
}

bool DataSharingInfo::isLocal(OMPExecutableDirective *S, VarDecl const *VD) {
  if (!S)
    return false;
  return !isShared(S, VD) || isLocal(nestedOmp.getFirstNode(S), VD);
}

bool DataSharingInfo::isGlobal(OMPExecutableDirective *S, VarDecl const *VD) {
  std::vector<VarDecl const *> Vars = getCapturedGlobalVars(S);
  return std::find(Vars.begin(), Vars.end(), VD) != Vars.end();
}

bool DataSharingInfo::wasShared(OMPExecutableDirective *S, VarDecl const *VD) {
  OMPExecutableDirective *parent = nestedOmp.getFirstNode(S);
  return parent && exists(parent, VD) &&
         (result[parent][VD] & DataSharing::DS_Shared);
}

bool DataSharingInfo::wasSharedSkipInline(OMPExecutableDirective *S, VarDecl const *VD) {
  OMPExecutableDirective *parent = nestedOmp.getFirstNode(S);
  while (parent != NULL && isInlineDirective(parent)) {
    parent = nestedOmp.getFirstNode(parent);
  }

  return parent && exists(parent, VD) &&
         (result[parent][VD] & DataSharing::DS_Shared);
}

bool DataSharingInfo::wasFirstPrivateTask(OMPExecutableDirective *S,
                                          VarDecl const *VD) {
  OMPExecutableDirective *parent = nestedOmp.getFirstNode(S);
  return parent && exists(parent, VD) && isa<OMPTaskDirective>(parent) &&
         (result[parent][VD] & DataSharing::DS_FirstPrivate);
}

bool DataSharingInfo::exists(OMPExecutableDirective *S, VarDecl const *VD) {
  return result.find(S) != result.end() &&
         result[S].find(VD) != result[S].end();
}

std::vector<VarDecl const *>
__getCapturedVarsOfFunctionTemplate(const FunctionTemplateDecl *FTD,
                                    OMPExecutableDirective *S) {
  std::vector<VarDecl const *> CapturedVars;

  for (auto *FD : FTD->specializations()) {
    if (FD) {
      CapturedVariablesVisitor Visitor(FD, S);
      CapturedVars = Visitor.getCapturedVars();
      break;
    }
  }
  return CapturedVars;
}

std::vector<VarDecl const *>
__getCapturedVarsOfCXXMethodDecl(const ClassTemplateDecl *CTD,
                                 const CXXMethodDecl *MD,
                                 OMPExecutableDirective *S) {
  std::vector<VarDecl const *> CapturedVars;

  for (auto *R : CTD->specializations()) {
    for (auto *M : R->methods()) {
      if (M->getLocation() == MD->getLocation()) {
        CapturedVariablesVisitor Visitor(M, S);
        CapturedVars = Visitor.getCapturedVars();
        return CapturedVars;
      }
    }
  }
  return CapturedVars;
}

void
DataSharingInfo::CaptureVariables(OMPExecutableDirective *S) {
  const CXXMethodDecl *MD =
    dyn_cast_or_null<CXXMethodDecl const>(__CurrentFunctionDecl);
  const ClassTemplateDecl *CTD = nullptr;
  std::vector<VarDecl const *> capturedVars;

  if (MD)
    CTD = MD->getParent()->getDescribedClassTemplate();

  if (CTD) {
    if (CTD->spec_begin() == CTD->spec_end()) {
      // Do no try to find captured variables inside a class template
      // which has no specializations.
      return;
    }

    LocalVariablesVisitor Locals(S);

    if (MD->isStatic() &&
        MD->getTemplatedKind() != FunctionDecl::TemplatedKind::TK_NonTemplate) {
      const FunctionTemplateDecl *FTD =
        __CurrentFunctionDecl->getDescribedFunctionTemplate();

      for (auto *R : CTD->specializations()) {
        for (auto *D : R->decls()) {
          FunctionTemplateDecl *FD = dyn_cast_or_null<FunctionTemplateDecl>(D);
          if (FD) {
            if (FD->getLocation() == FTD->getLocation()) {
              for (auto *U : FD->specializations()) {
                if (U->getLocation() == MD->getLocation()) {
                  CapturedVariablesVisitor Captured(U, S);
                  capturedVars = Captured.getCapturedVars();
                }
              }
            }
          }
        }
      }
    } else {
      capturedVars = __getCapturedVarsOfCXXMethodDecl(CTD, MD, S);
    }

    for (unsigned i = 0; i < capturedVars.size(); i++) {
      for (auto *VD : Locals.getCapturedVars()) {
        if (capturedVars[i] &&
            capturedVars[i]->getLocation() == VD->getLocation()) {
          capturedVars[i] = VD;
        }
      }
    }
  } else if (const FunctionTemplateDecl *FTD =
             __CurrentFunctionDecl->getDescribedFunctionTemplate()) {
    if (FTD->spec_begin() == FTD->spec_end()) {
      // Do no try to find captured variables inside a function template
      // which has no specializations.
      return;
    }

    // Special case for function template declarations.
    LocalVariablesVisitor Locals(S);

    capturedVars = __getCapturedVarsOfFunctionTemplate(FTD, S);
    for (unsigned i = 0; i < capturedVars.size(); i++) {
      for (auto *VD : Locals.getCapturedVars()) {
        if (capturedVars[i] &&
            capturedVars[i]->getLocation() == VD->getLocation()) {
          capturedVars[i] = VD;
        }
      }
    }
  } else {
    // Get captured variables of the current OpenMP directive.
    capturedVars = GenUtils::getCapturedVars(S);

    // Static local variables are not captured by the CapturedStmt object
    // which is tied to each OpenMP directive.
    StaticLocalVariablesVisitor StaticLocals(S);
    for (auto *VD : StaticLocals.getCapturedVars()) {
      SourceLocation L = VD->getLocation();
      if (L.getPtrEncoding() < S->getLocStart().getPtrEncoding()) {
        // Only capture static local variables which are declared outside
        // of the enclosing context.
        capturedVars.insert(capturedVars.end(), VD);
      }
    }
  }

  // Insert global captured variables which are referenced inside
  // the OpenMP directive context.
  for (auto &VD : getCapturedGlobalVars(S)) {
    if (std::find(capturedVars.begin(), capturedVars.end(), VD)
        == capturedVars.end())
      capturedVars.insert(capturedVars.end(), VD);
  }

  // Insert copyin and private variables.
  for (auto &it : result[S]) {
    if (it.second & DataSharing::DS_CopyIn ||
        it.second & DataSharing::DS_Private) {
      if (std::find(capturedVars.begin(), capturedVars.end(), it.first)
          == capturedVars.end()) {
        capturedVars.push_back(it.first);
      }
    }
  }

  __CapturedVars[S] = capturedVars;
}

const std::vector<VarDecl const *> &
DataSharingInfo::getCapturedVars(OMPExecutableDirective *S, bool withDSA) {
  return withDSA ? __CapturedVarsDSA[S] : __CapturedVars[S];
}

bool DataSharingInfo::isVarCaptured(OMPExecutableDirective *S,
                                    VarDecl const *VD) {
  const std::vector<VarDecl const *> CapturedVars = getCapturedVars(S, true);

  for (auto *V : CapturedVars) {
    if (!V)
      continue;

    if (V->getLocation() == VD->getLocation())
      return true;
  }

  return false;
}

bool DataSharingInfo::emptyOrOnlyPrivate(OMPExecutableDirective *S) {
  auto iter = getCapturedVars(S, true);
  // nullptr == this
  return std::none_of(iter.begin(), iter.end(), [=](VarDecl const *VD) {
    return not VD or !isPrivate(S, VD);
  });
}

const std::vector<VarDecl const *> &
DataSharingInfo::getCapturedGlobalVars(OMPExecutableDirective *S) const {
  assert(__CapturedGlobalVars.find(S) != __CapturedGlobalVars.end());
  return __CapturedGlobalVars.find(S)->second;
}

void DataSharingInfo::CaptureGlobalVariables(OMPExecutableDirective *S) {
  CapturedStmt *CS = cast<CapturedStmt>(S->getAssociatedStmt());
  GlobalVariablesVisitor Visitor(CS);

  std::vector<VarDecl const *> GlobalVars(Visitor.getCapturedVars().begin(),
                                          Visitor.getCapturedVars().end());
  __CapturedGlobalVars[S] = GlobalVars;
}

bool DataSharingInfo::isInlineDirective(OMPExecutableDirective *S) {
  return isa<OMPCriticalDirective>(S) || isa<OMPMasterDirective>(S) ||
         isa<OMPOrderedDirective>(S) || isa<OMPTaskgroupDirective>(S);
}

const FunctionDecl *
DataSharingInfo::getFunctionDecl(OMPExecutableDirective *S) {
  return __FunctionDecl[S];
}

OMPExecutableDirective *
DataSharingInfo::getParent(OMPExecutableDirective *S) {
  return nestedOmp.getFirstNode(S);
}
