#include "analyses/OMPGraph.h"

using namespace clang;

OMPGraph::OMPGraph(Decl const *s) { TraverseDecl(const_cast<Decl *>(s)); }

void OMPGraph::ProcessOMPExecutableDirective(OMPExecutableDirective *S) {
  if (visited_.count(S) == 0) {
    if (!parents_.empty()) {
      result[parents_.top()].second.push_back(S);
      result[S].first = parents_.top();
    } else {
      result[S]; /* Touch result so that "S" entry is created*/
    }
    parents_.push(S);
    for (Stmt *Child : S->children())
      TraverseStmt(Child);
    parents_.pop();
    visited_.insert(S);
  }
}
