#include <llvm/Support/raw_ostream.h>

#include <algorithm>

#include "codegen/Util.h"
#include "MultiDimArray.h"
#include "StarPU.h"
#include "StarPUPragma.h"
#include "GenUtils.h"
#include "Printer.h"

using namespace clang;

StarPUTarget::StarPUTarget(OMPTargetDirective *t, ASTContext &c,
                           DataSharingInfo &dsi)
    : RuntimeTarget(t, c, dsi) {}

std::string StarPUTarget::genBeforeFunction(std::queue<std::string> &) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << genDeclareArgStruct(argStruct_);

  if (!GenUtils::getTopCXXMethodDecl(getOMPDirective()->getAssociatedStmt(),
                                     getContext())) {
    // Only generate signatures when the given OpenMP pragma is
    // not located inside a CXX method.
    rso << getRuntime()->genTaskSignature(getContext(), "_cpu_" + newFunction_,
                                          addInjectedTemplateArgs(true)) << ";\n";
    rso << getRuntime()->genTaskSignature(getContext(), "_gpu_" + newFunction_,
                                          addInjectedTemplateArgs(true)) << ";\n";
  }

  return rso.str();
}

std::string StarPUTarget::genAfterFunction(std::queue<std::string> &q) {
  std::string str;
  llvm::raw_string_ostream rso(str);

  rso << genFuncWrapper(q, "_cpu_");
  rso << genFuncWrapper(q, "_gpu_");

  return rso.str();
}

std::string StarPUTarget::genReplacePragma(std::queue<std::string> &) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  std::string attr_var = "attr_" + std::to_string(getId());
  std::string cond = "1";
  CallExpr *call = cast<CallExpr>(captured_->getCapturedStmt());
  rso << "struct starpu_omp_task_region_attr " << attr_var << " = {};\n"
      << attr_var << ".cl.cpu_funcs[0]  = " << "_cpu_" << newFunction_ << ";\n"
      << "if(_gpu_" << call->getDirectCallee()->getName()
      << ")" << attr_var << ".cl.cuda_funcs[0]  = _gpu_" << newFunction_
      << ";\n" << attr_var << ".if_clause        = " << cond << ";\n"
      << attr_var << ".cl.name = "
      << "\"" << GenUtils::getTaskName(getOMPDirective(), getContext()) << "\""
      << ";\n" << attr_var << ".cl.cuda_flags[0] = STARPU_CUDA_ASYNC;\n";
  int nbrBuffer_ = 0;
  for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
    if (std::find(by_ref.begin(), by_ref.end(), param->getName()) ==
        by_ref.end()) // Not in by_ref
      continue;

    for (unsigned i = 0; i < to_[param].size(); i++) {
      OpenMPMapClauseKind Kind = OpenMPMapClauseKind::OMPC_MAP_to;
      rso << attr_var << ".cl.modes[" << nbrBuffer_++ << "] = "
          << getRuntime()->EmitMapType(Kind) << ";";
    }
    for (unsigned i = 0; i < from_[param].size(); i++) {
      OpenMPMapClauseKind Kind = OpenMPMapClauseKind::OMPC_MAP_from;
      rso << attr_var << ".cl.modes[" << nbrBuffer_++ << "] = "
          << getRuntime()->EmitMapType(Kind) << ";";
    }
    for (unsigned i = 0; i < alloc_[param].size(); i++) {
      OpenMPMapClauseKind Kind = OpenMPMapClauseKind::OMPC_MAP_alloc;
      rso << attr_var << ".cl.modes[" << nbrBuffer_++ << "] = "
          << getRuntime()->EmitMapType(Kind) << ";";
    }

    // FIXME: Special case!
    size_t size = std::max<unsigned long>(1ul, toFrom_[param].size());
    for (size_t i = 0; i < size; i++) {
      OpenMPMapClauseKind Kind = OpenMPMapClauseKind::OMPC_MAP_tofrom;
      rso << attr_var << ".cl.modes[" << nbrBuffer_++ << "] = "
          << getRuntime()->EmitMapType(Kind) << ";";
    }
  }

  rso << attr_var << ".cl.nbuffers = " << nbrBuffer_ << ";\n";

  std::string params = "params_" + std::to_string(getId());
  rso << "\t\tstarpu_data_handle_t handles[" << nbrBuffer_ << "];\n";
  rso << attr_var << ".handles = &handles[0];\n";

  if (!dsi.emptyOrOnlyPrivate(getOMPDirective())) {
    // FILL CL_ARG
    CodeGen::StructInit argStruct =
        genFillArgStruct(params, argStruct_, &by_ref);
    argStruct.setDynalloc(
        CodeGen::GenMalloc(CodeGen::GenSizeof("struct " + argStruct_)));
    rso << argStruct.str();
    rso << attr_var << ".cl_arg_free = 1;\n";
    rso << attr_var
        << ".cl_arg_size = " + CodeGen::GenSizeof("struct " + argStruct_) +
               ";\n";
    if (!argStruct.str().empty())
      rso << attr_var << ".cl_arg = " << params << ";\n";

    int bufInd = 0;
    // FILL BUFFERS
    for (VarDecl const *param : dsi.getCapturedVars(getOMPDirective())) {
      if (std::find(by_ref.begin(), by_ref.end(), param->getName()) ==
          by_ref.end()) // in by_ref
        continue;

      for (Expr const *b : to_[param]) {
        rso << getRuntime()->genRegister(b, param, dsi, getOMPDirective(), bufInd,
                                         getContext());
        bufInd++;
      }
      for (Expr const *b : from_[param]) {
        rso << getRuntime()->genRegister(b, param, dsi, getOMPDirective(), bufInd,
                                         getContext());
        bufInd++;
      }
      for (Expr const *b : alloc_[param]) {
        rso << getRuntime()->genRegister(b, param, dsi, getOMPDirective(), bufInd,
                                         getContext(), true);
        bufInd++;
      }

      // FIXME: Special case!
      bool intofrom = toFrom_.find(param) != toFrom_.end();
      if (intofrom) {
        if (toFrom_[param].empty()) {

          rso << "if(" << getStarPURuntime()->starpu_omp_data_lookup_func() << "("
              << GenUtils::getAddrVar(param, dsi, getOMPDirective()) << "))\n";
          rso << "\thandles[" << bufInd << "] = "
              << getStarPURuntime()->starpu_omp_data_lookup_func() << "("
              << GenUtils::getAddrVar(param, dsi, getOMPDirective()) << ");\n";
          rso << "else {\n";
          rso << "\tstarpu_variable_data_register(&handles[" << bufInd++
              << "], 0, (uintptr_t)"
              << GenUtils::getAddrVar(param, dsi, getOMPDirective()) << ", "
              << CodeGen::GenSizeof(GenUtils::getVar(param, dsi, getOMPDirective()))
              << ");\n";
          if (getStarPURuntime()->p_has_starpu_omp_data_lookup()) {
              rso << "starpu_omp_handle_register(handles["
                  << bufInd
                  << "]);\n";
          }
          rso << "}\n";
        } else
          for (Expr const *b : toFrom_[param]) {
            rso << getRuntime()->genRegister(b, param, dsi, getOMPDirective(), bufInd,
                                           getContext());
            bufInd++;
          }
      }
    }
    assert(bufInd == nbrBuffer_);
  }
  rso << "\tstarpu_omp_task_region(&" << attr_var << ");\n";
  if (not nowait_)
    rso << getRuntime()->EmitTaskwait();
  return rso.str();
}

std::string StarPUTarget::genExpandDMA(std::unordered_map<VarDecl const *,
                                       std::set<Expr const *>> &map,
                                       VarDecl const *param, size_t &j) {
  std::string str;
  llvm::raw_string_ostream rso(str);
  if (map.find(param) != map.end()) {
    size_t len = std::max<unsigned long>(1ul, map[param].size());
    for (size_t i = 0; i < len; i++, j++) {
      rso << param->getType().getAsString() << param->getNameAsString()
          << " = (" << param->getType().getAsString() << ")"
          << "STARPU_VARIABLE_GET_PTR(kstar_buffers[" << j << "]);";
    }
  }
  return rso.str();
}

std::string StarPUTarget::genFuncWrapper(std::queue<std::string> &q,
                                         std::string prefix)
{
  std::string WrapperFuncName = prefix + newFunction_;
  std::string dummy1;
  llvm::raw_string_ostream streamFalse(dummy1);
  KStarStmtPrinter printerFalse(streamFalse, getContext().getPrintingPolicy(),
                                q, &dsi, getOMPDirective());
  std::string str;
  llvm::raw_string_ostream rso(str);
  CallExpr *call;

  if (!isa<CallExpr>(captured_->getCapturedStmt()))
    return "";
  call = cast<CallExpr>(captured_->getCapturedStmt());

  // Generate function declaration.
  rso << getRuntime()->genTaskSignature(getContext(), WrapperFuncName,
                                        addInjectedTemplateArgs(true))
      << "{\n";

  // Generate code which retrieves parameters from the codelet args.
  CodeGen::StructExpand argStruct = expandArgStruct("kstar_params", argStruct_);
  argStruct.except(by_ref);
  rso << argStruct.str();

  // Generate code which retrieves parameters from codelet buffers.
  size_t j = 0;
  for (const VarDecl *VD : dsi.getCapturedVars(getOMPDirective(), true)) {
    if (std::find(by_ref.begin(), by_ref.end(), VD->getName()) == by_ref.end())
      continue;
    rso << genExpandDMA(to_, VD, j);
    rso << genExpandDMA(from_, VD, j);
    rso << genExpandDMA(alloc_, VD, j);
    rso << genExpandDMA(toFrom_, VD, j);
  }

  // Rewrite how parameters are passed to this function.
  rso << prefix << call->getDirectCallee()->getName() << "(";

  bool first = true;
  for (auto iter = call->arg_begin(); iter != call->arg_end(); ++iter) {
    if (not first)
      rso << ",";
    else
      first = false;

    const VarDecl *VD = GenUtils::getVarDeclFromExpr(*iter);
    if (std::find(by_ref.begin(), by_ref.end(), VD->getName()) != by_ref.end()) {
      std::string tmp_str;
      llvm::raw_string_ostream tmp_rso(str);
      StmtPrinter P(rso, 0, getContext().getPrintingPolicy());
      P.PrintExpr(*iter);
      rso << tmp_rso.str();
    } else {
      streamFalse.str().clear();
      printerFalse.PrintExpr(*iter);
      rso << streamFalse.str();
    }
  }
  rso << ");\n}\n";

  return rso.str();
}
