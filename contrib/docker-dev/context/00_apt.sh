#!/bin/sh
set -e
apt-get update
apt-get install -y --no-install-recommends ca-certificates automake autoconf clang cmake git hwloc libhwloc-common libhwloc-dev libtool libtool-bin m4 make pkg-config python3
rm -rf /var/lib/apt/lists/*
