#!/bin/sh
set -e
mkdir -p /tmp/src
cd /tmp/src
git clone --single-branch -b release_39 https://github.com/llvm-mirror/llvm.git
git clone --single-branch -b release_39 https://gitlab.inria.fr/kstar/clang-omp.git
mkdir -p /tmp/build-llvm
cd /tmp/build-llvm
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/llvm -DLLVM_TARGETS_TO_BUILD="X86" -DLLVM_EXTERNAL_CLANG_SOURCE_DIR=/tmp/src/clang-omp /tmp/src/llvm
make -j 4
make install
rm -rf /tmp/src
rm -rf /tmp/build-llvm
