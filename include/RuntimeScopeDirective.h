#ifndef KSTAR_RUNTIMESCOPEDIRECTIVE_H
#define KSTAR_RUNTIMESCOPEDIRECTIVE_H

#include <clang/AST/StmtOpenMP.h>
#include <clang/AST/TemplateBase.h>
#include <clang/AST/ASTContext.h>

#include "analyses/DSA.h"
#include "RuntimeDirective.h"
#include "codegen/StructInit.h"
#include "codegen/StructExpand.h"
#include "codegen/Util.h"

class RuntimeScopeDirective : public RuntimeDirective {
public:
  RuntimeScopeDirective(clang::OMPExecutableDirective *oed,
                        clang::ASTContext &c, DataSharingInfo &dsi);
  bool haveReduction();
  virtual CodeGen::StructInit
  genFillArgStruct(std::string const &argStructDest,
                   std::string const &argStructType,
                   std::vector<std::string> *by_ref = nullptr);
  CodeGen::StructExpand expandArgStruct(std::string const &argStructName,
                                        std::string const &argStructType);

protected:
  virtual std::string genDeclareExtraArgStruct();
  std::string genDeclareArgStruct(std::string const &argStructType);
  std::string genReductionMinLimit(const clang::VarDecl *VD);
  std::string genReductionMaxLimit(const clang::VarDecl *VD);
  std::string genReductionInitializer(const clang::VarDecl *VD);
  std::string genReductionCombiner(std::string const &argStructName,
                                   std::string const &argStructType);
  std::string genForOutlinedFunction(std::string &newFunction,
                                     std::string &argStruct, int collapse,
                                     std::string const &body,
                                     bool with_for_decl = true);
  std::string getNumThreads(clang::Expr *numThreads, clang::Expr *ifClause);

  const std::string addInjectedTemplateArgs(bool withTypename = false) const {
    return CodeGen::genInjectedTemplateArgs(
        templateArgs_, getContext().getPrintingPolicy(), withTypename);
  }

  void ActOnOpenMPNowaitClause(clang::OMPNowaitClause *);
  void ActOnOpenMPNumThreadsClause(clang::OMPNumThreadsClause *);
  void ActOnOpenMPIfClause(clang::OMPIfClause *);
  void ActOnOpenMPProcBindClause(clang::OMPProcBindClause *);
  void ActOnOpenMPWhereClause(clang::OMPWhereClause *);
  void ActOnOpenMPCopyinClause(clang::OMPCopyinClause *);
  void ActOnOpenMPCopyprivateClause(clang::OMPCopyprivateClause *);
  void ActOnOpenMPOrderedClause(clang::OMPOrderedClause *);
  void ActOnOpenMPScheduleClause(clang::OMPScheduleClause *);
  void ActOnOpenMPFinalClause(clang::OMPFinalClause *);
  void ActOnOpenMPMergeableClause(clang::OMPMergeableClause *);
  void ActOnOpenMPUntiedClause(clang::OMPUntiedClause *);
  void ActOnOpenMPPriorityClause(clang::OMPPriorityClause *);
  void ActOnOpenMPMapClause(clang::OMPMapClause *);
  void ActOnOpenMPTaskNameClause(clang::OMPTaskNameClause *);
  void ActOnOpenMPDependClause(clang::OMPDependClause *);

  bool hasInjectedTemplateArgs() const { return !templateArgs_.empty(); }

  /// \brief Return a FunctionTemplateDecl object if template paremeters have
  /// been captured for this OpenMP directive.
  ///
  /// \return nullptr is returned if template parameters are unused.
  clang::FunctionTemplateDecl *
  CreateFunctionTemplate(clang::FunctionDecl *FD) const;

  DataSharingInfo &dsi;
  clang::CapturedStmt *captured_;
  std::string newFunction_;
  std::string argStruct_;
  llvm::ArrayRef<clang::TemplateArgument> templateArgs_;

  bool nowait_;
  clang::Expr *numThreads_;
  clang::Expr *ifClause_;
  clang::Expr *finalCond_;
  bool mergeCond_;
  clang::OpenMPProcBindClauseKind procBind_;
  clang::OpenMPWhereClauseKind where_;
  clang::OMPCopyinClause *copyIn_;
  clang::OpenMPScheduleClauseKind schedule_;
  clang::Expr *chunkSize_;
  bool ordered_;
  bool untiedCond_;
  clang::Expr *priority_;
  clang::Expr *name_;

  std::vector<clang::VarDecl const *> copyPrivate_;
  std::unordered_map<clang::VarDecl const *, std::set<clang::Expr const *>>
      from_;
  std::unordered_map<clang::VarDecl const *, std::set<clang::Expr const *>>
      toFrom_;
  std::unordered_map<clang::VarDecl const *, std::set<clang::Expr const *>> to_;
  std::unordered_map<clang::VarDecl const *, std::set<clang::Expr const *>>
      alloc_;
  std::unordered_map<clang::VarDecl const *, std::set<clang::Expr const *>>
      inDeps_;
  std::unordered_map<clang::VarDecl const *, std::set<clang::Expr const *>>
      outDeps_;
  std::unordered_map<clang::VarDecl const *, std::set<clang::Expr const *>>
      cwDeps_;
  std::unordered_map<clang::VarDecl const *, std::set<clang::Expr const *>>
      commuteDeps_;
private:
  bool __isLocalStructDecl(clang::VarDecl const *D);
};
#endif
