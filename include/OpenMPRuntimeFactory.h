//===----------------------------------------------------------------------===//
//
// This provides a singleton factory class for creating OpenMP runtimes.
//
//===----------------------------------------------------------------------===//

#ifndef KSTAR_OPENMPRUNTIMEFACTORY_H
#define KSTAR_OPENMPRUNTIMEFACTORY_H

#include "OpenMPRuntime.h"

class OpenMPRuntimeFactory {
  OpenMPRuntimeFactory
  &operator=(const OpenMPRuntimeFactory &) = delete;
  OpenMPRuntimeFactory(const OpenMPRuntimeFactory &) = delete;
  OpenMPRuntimeFactory() = delete;
  ~OpenMPRuntimeFactory() = delete;

  /// \brief Current instance of the OpenMP runtime.
  static OpenMPRuntime *Instance;

public:
  enum OpenMPRuntimeType {
    STARPU,
    KAAPI,
  };

  /// \brief Build an OpenMP runtime.
  static void build(enum OpenMPRuntimeType T, std::string version);

  /// \brief Return the current instance of the OpenMP runtime.
  static OpenMPRuntime *getInstance() {
    return Instance;
  }
};

#endif
