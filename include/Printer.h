#ifndef KSTAR_PRINTER_H
#define KSTAR_PRINTER_H

#include <clang/AST/StmtPrinter.h>
#include <clang/AST/DeclPrinter.h>
#include <clang/AST/TypePrinter.h>

#include <queue>
#include <string>

#include "analyses/DSA.h"

class KStarDeclPrinter;
class KStarTypePrinter;

class KStarStmtPrinter : public clang::StmtPrinter {
  friend KStarDeclPrinter;
  friend KStarTypePrinter;
  std::queue<std::string> &replacements_;
  DataSharingInfo *dsi_;
  clang::OMPExecutableDirective *ompDirective_;
  bool parent_;
  bool IsInner_;
  std::vector<std::string> ByRef_;
  KStarTypePrinter *type_printer;

public:
  KStarStmtPrinter(llvm::raw_ostream &os, const clang::PrintingPolicy &Policy,
                   std::queue<std::string> &q, DataSharingInfo *dsi = nullptr,
                   clang::OMPExecutableDirective *omp_directive = nullptr,
                   bool parent = false, bool IsInner = false,
                   std::vector<std::string> ByRef = std::vector<std::string>());
  virtual ~KStarStmtPrinter();
  void PrintRawDecl(clang::Decl *D) override;
  void PrintRawDeclStmt(const clang::DeclStmt *S) override;
  void VisitDeclRefExpr(clang::DeclRefExpr *Node) override;
  virtual void VisitCXXNamedCastExpr(clang::CXXNamedCastExpr *E) /* XXX*/;
  void VisitCXXStaticCastExpr(clang::CXXStaticCastExpr *Node) override;
  void VisitCXXDynamicCastExpr(clang::CXXDynamicCastExpr *Node) override;
  void VisitCXXReinterpretCastExpr(clang::CXXReinterpretCastExpr *Node) override;
  void VisitCXXConstCastExpr(clang::CXXConstCastExpr *Node) override;
  void VisitCXXFunctionalCastExpr(clang::CXXFunctionalCastExpr *Node) override;
  void VisitCXXNewExpr(clang::CXXNewExpr *E) override;
  void VisitCXXThisExpr(clang::CXXThisExpr *Node) override;
  void VisitOMPAtomicDirective(clang::OMPAtomicDirective *Node) override;
#define GENERATE(PRAGMA)                                                       \
  void VisitOMP##PRAGMA##Directive(clang::OMP##PRAGMA##Directive *s) override;
#include <pragma.def>
};

class KStarDeclPrinter : public clang::DeclPrinter {
private:
  KStarStmtPrinter *sp_ = nullptr;
  KStarTypePrinter *tp_ = nullptr;

public:
  KStarDeclPrinter(KStarStmtPrinter *SP, KStarTypePrinter *TP)
      : DeclPrinter(SP->OS, SP->Policy), sp_(SP), tp_(TP) {}
  void printGroup(clang::Decl **Begin, unsigned NumDecls);
  void VisitVarDecl(clang::VarDecl *D) override;
};

class KStarTypePrinter : public clang::TypePrinter {
  friend KStarDeclPrinter;
  friend KStarStmtPrinter;
  KStarStmtPrinter *sp_ = nullptr;

public:
  explicit KStarTypePrinter(KStarStmtPrinter *sp)
      : TypePrinter(sp->Policy), sp_(sp) {}
  virtual ~KStarTypePrinter() {}
  void printVariableArrayAfter(const clang::VariableArrayType *T,
                               llvm::raw_ostream &OS) override;
};
#endif
