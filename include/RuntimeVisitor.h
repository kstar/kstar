#ifndef KSTAR_RUNTIMEVISITOR_H
#define KSTAR_RUNTIMEVISITOR_H

#include "clang/Basic/LLVM.h"

#include <clang/AST/ASTConsumer.h>
#include <clang/AST/ASTContext.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/AST/StmtOpenMP.h>
#include <clang/Driver/Options.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/FrontendActions.h>
#include <clang/Lex/PPCallbacks.h>
#include <clang/Lex/Preprocessor.h>
#include <clang/Rewrite/Core/Rewriter.h>
#include <clang/Rewrite/Frontend/Rewriters.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include <clang/Tooling/Tooling.h>

#include <set>
#include <queue>
#include <vector>
#include <unordered_set>

#include "analyses/OMPGraph.h"
#include "analyses/DSA.h"
#include "OpenMPRuntime.h"
#include "OpenMPRuntimeFactory.h"
#include "GenUtils.h"
#include "context.h"

/*
 * RecursiveASTVisitor is the big-kahuna visitor that traverses
 * everything in the AST.
 * It does the transformation using the rewriter_ argument.
 */
class RuntimeVisitor : public clang::RecursiveASTVisitor<RuntimeVisitor> {
public:
  RuntimeVisitor(clang::Rewriter &R, clang::ASTContext &C);

#define WRAP(PRAGMA)                                                           \
  bool VisitOMP##PRAGMA##Directive(clang::OMP##PRAGMA##Directive *S) {         \
    ProcessWrapOMPExecutableDirective(S);                                      \
    return true;                                                               \
  }
#include "pragma.def"

#define LOCAL(PRAGMA)                                                          \
  bool VisitOMP##PRAGMA##Directive(clang::OMP##PRAGMA##Directive *S) {         \
    ProcessLocalOMPExecutableDirective(S);                                     \
    return true;                                                               \
  }
#include "pragma.def"

  /*
   * Transform variable declared as thread Private adding the "__thread"
   * type annotation.
   */
  bool VisitOMPThreadPrivateDecl(clang::OMPThreadPrivateDecl *);

  bool VisitVarDecl(clang::VarDecl *);

  /// \brief Visit all function declarations.
  ///
  /// This is currently only used for target declare constructs.
  bool VisitFunctionDecl(clang::FunctionDecl *);

  /*
   * Add code before and after pragma can be done only at then end of the
   * Translation Unit.
   * Also, add the __attribute(constructor) and __attribute(destructor)
   */
  void endOfTranslationUnit();
  /*
   * Add setter as these variables can't be computed in the constructor
   * because they are based on TranslationUnitDecl which doesn't exists
   * yet
   */
  void setDataSharing(DataSharingInfo *d) { dsi_ = d; }

  /// \brief Return the current AST context.
  clang::ASTContext &getContext() const {
    return Context;
  }

  /// \brief Return the current rewriter.
  clang::Rewriter &getRewriter() const {
    return Rewrite;
  }

  /// \brief Return the current OpenMP runtime.
  OpenMPRuntime *getRuntime() const {
    return OpenMPRuntimeFactory::getInstance();
  }

private:
  /// PRIVATE FUNCTIONS

  /// \brief This method processes OpenMP executable directives which have an
  /// enclosing context.
  void ProcessWrapOMPExecutableDirective(clang::OMPExecutableDirective *S);

  /// \brief This method processes stand-alone OpenMP executable directives.
  void ProcessLocalOMPExecutableDirective(clang::OMPExecutableDirective *S);

  /*
   * Gather all replacement part and finally remplace once all nested
   * pragma are handle
   */
  void spawnDirective(RuntimeDirective *);

  /// \brief Return whether a pragma is located inside an empty template decl.
  bool isInUnusedTemplateDecl(clang::OMPExecutableDirective *S);

  /// PRIVATE ATTRIBUTS

  /// \brief Clang's rewriter.
  clang::Rewriter &Rewrite;

  /// \brief Clang's AST context.
  clang::ASTContext &Context;

  /*
   * Contains information to be generated for given RuntimeScopeDirective
   * as we can't do this step by step, we gather information and do it
   * once in a row at the end of visit.
   */
  std::vector<std::tuple<RuntimeDirective *, std::string, std::string>>
      postWrap_;
  /*
   * Temporary object to store code that have be generate before function
   * for the current nested pragma stmts
   */
  std::string beforeFunction_;
  /*
   * Temporary object to store code that have be generate after function
   * for the current nested pragma stmts
   */
  std::string afterFunction_;
  /*
   * Temporary stacked pragma replacement to rewrite the pragma once in a
   * row at the end
   */
  std::vector<std::queue<std::string>> replacePragma_;
  /*
   * Save already visited node in the AST as we don't want postfix visit
   * of the AST (Hacky way to do this ...)
   */
  std::unordered_set<clang::OMPExecutableDirective *> visited_;
  /*
   * List of named mutex used in the programm
   */
  std::set<std::string> mutexNames_;
  /*
   * DataSharing information for the whole programm
   */
  DataSharingInfo *dsi_;
  /*
   * nested depth of the current pragma
   */
  unsigned int pragmaDepth_;

  /// \brief List of captured global variables.
  std::set<clang::VarDecl *> GlobalVariables;
};

/**
 * Implementation of the ASTConsumer interface for reading an AST produced
 * by the Clang parser.
 */
class RuntimeConsumer : public clang::ASTConsumer {
public:
  RuntimeConsumer(clang::Rewriter &R, clang::ASTContext &C) : visitor_(R, C) {}
  /*
   * Start to visit the whole TranslationUnit
   */
  void HandleTranslationUnit(clang::ASTContext &) override;

private:
  RuntimeVisitor visitor_;
};

class RewriterFrontendAction : public clang::ASTFrontendAction {
public:
    std::unique_ptr<clang::ASTConsumer>
    CreateASTConsumer(clang::CompilerInstance &CI,
                      clang::StringRef file) override {
    Acontext.rewriter_->setSourceMgr(CI.getSourceManager(), CI.getLangOpts());
    // RuntimeConsumer will be deleted by the framework calling us.
    return llvm::make_unique<RuntimeConsumer>(*Acontext.rewriter_,
                                              CI.getASTContext());
  };
};

#endif
