#ifndef KSTAR_ANALYSES_DSA_H
#define KSTAR_ANALYSES_DSA_H

#include <clang/AST/RecursiveASTVisitor.h>

#include <array>
#include <set>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "analyses/OMPGraph.h"

class DataSharingInfo : public clang::RecursiveASTVisitor<DataSharingInfo> {
  OMPGraph nestedOmp;
  clang::ASTContext &Context;

  /// \brief Data sharing flag attributes.
  enum DataSharing {
    DS_Private      = 1,
    DS_Shared       = 2,
    DS_FirstPrivate = 4,
    DS_LastPrivate  = 8,
    DS_Reduce       = 16,
    DS_CopyIn       = 32,
  };

  /// \brief List of ThreadPrivate variables.
  std::set<clang::VarDecl const *> __ThreadPrivateVars;

  /// \brief Hashmap of captured global variables for each OpenMP directives.
  std::unordered_map<clang::OMPExecutableDirective *,
                     std::vector<clang::VarDecl const *>> __CapturedGlobalVars;

  /// \brief Hashmap of function declarations for each OpenMP directives.
  std::unordered_map<clang::OMPExecutableDirective *,
                     clang::FunctionDecl const *> __FunctionDecl;

  /// \brief Pointer to the current function declaration while visiting AST.
  const clang::FunctionDecl *__CurrentFunctionDecl;

  /// \brief Hashamp of captured variables for each OpenMP directives.
  /// TODO: Merge these two hashmaps!
  std::unordered_map<clang::OMPExecutableDirective *,
                     std::vector<clang::VarDecl const *>> __CapturedVars;
  std::unordered_map<clang::OMPExecutableDirective *,
                     std::vector<clang::VarDecl const *>> __CapturedVarsDSA;
  std::unordered_map<clang::OMPExecutableDirective *,
                     std::unordered_map<clang::VarDecl const *, unsigned>> result;

  /// \brief Process implicit data sharing information.
  void implicitDataSharing(clang::VarDecl const *VD,
                           std::vector<clang::VarDecl *> iterDecl,
                           clang::OMPExecutableDirective *S,
                           bool defaultShared);

  /// \brief Return whether this variable declaration exists.
  bool exists(clang::OMPExecutableDirective *S, clang::VarDecl const *VD);

  /// \brief Return whether this variable declaration is local.
  bool isLocal(clang::OMPExecutableDirective *S, clang::VarDecl const *VD);

  /// \brief Return whether this variable declaration is global.
  bool isGlobal(clang::OMPExecutableDirective *S, clang::VarDecl const *VD);

  /// \brief Return whether this variable declaration is threadprivate.
  bool isThreadPrivateVar(clang::VarDecl const *VD);

  /// \brief Get the list of captured global variables for a directive.
  const std::vector<clang::VarDecl const *> &
  getCapturedGlobalVars(clang::OMPExecutableDirective *S) const;

  /// \brief Capture global variables.
  ///
  /// This method uses the GlobalVariablesVisitor visitor object to find
  /// all global variables which are referenced by the context of the
  /// given OpenMP directive.
  void CaptureGlobalVariables(clang::OMPExecutableDirective *S);

  /// \brief Capture all variables.
  ///
  /// This method captures all variables including global, copyin, etc.
  void CaptureVariables(clang::OMPExecutableDirective *S);

  /// \brief Process supported OpenMP executable directives.
  void ProcessOMPExecutableDirective(clang::OMPExecutableDirective *S);

public:
  DataSharingInfo(const clang::Decl *D, clang::ASTContext &C);

#define WRAP(PRAGMA)                                                           \
  bool VisitOMP##PRAGMA##Directive(clang::OMP##PRAGMA##Directive *S) {         \
    ProcessOMPExecutableDirective(S);                                          \
    return true;                                                               \
  }
#include "pragma.def"

  /// \brief Keep a trace of ThreadPrivate variables.
  bool VisitOMPThreadPrivateDecl(clang::OMPThreadPrivateDecl *);

  /// \brief Visit function declarations to store the current one.
  bool VisitFunctionDecl(clang::FunctionDecl const *FD);

  /// \brief Return whether this variable declaration is shared.
  bool isShared(clang::OMPExecutableDirective *S, clang::VarDecl const *VD);

  /// \brief Return whether this variable declaration is reduced.
  bool isReduce(clang::OMPExecutableDirective *S, clang::VarDecl const *VD);

  /// \brief Return whether this variable declaration is private.
  bool isPrivate(clang::OMPExecutableDirective *S, clang::VarDecl const *VD);

  /// \brief Return whether this variable declaration is last private.
  bool isLastPrivate(clang::OMPExecutableDirective *S,
                     clang::VarDecl const *VD);

  /// \brief Return whether this variable declaration is first private.
  bool isFirstPrivate(clang::OMPExecutableDirective *S,
                      clang::VarDecl const *VD);

  /// \brief Return whether this variable declaration is copy in.
  bool isCopyIn(clang::OMPExecutableDirective *S, clang::VarDecl const *VD);

  /// \brief Return TRUE when the directive has access to 'this' pointer.
  bool isInner(clang::OMPExecutableDirective *S);

  /// \brief Return whether this variable declaration is captured.
  bool isVarCaptured(clang::OMPExecutableDirective *S,
                     clang::VarDecl const *VD);

  /// \brief Return whether this OpenMP directive has reduce operators.
  bool hasReduce(clang::OMPExecutableDirective *S);

  /// \brief Return whether this OpenMP directives has last private variables.
  bool hasLastPrivate(clang::OMPExecutableDirective *S);

  /// \brief Return the reduce operator of this variable declaration.
  const std::string
  getReduceOperatorStr(clang::OMPExecutableDirective *, clang::VarDecl const *);

  /// \brief Get the list of all captured variables.
  const std::vector<clang::VarDecl const *> &
  getCapturedVars(clang::OMPExecutableDirective *S, bool withDSA = false);

  /// \brief Get the function decl where this OpenMP pragma is located.
  const clang::FunctionDecl *getFunctionDecl(clang::OMPExecutableDirective *S);

  /// TODO
  bool wasShared(clang::OMPExecutableDirective *S, clang::VarDecl const *VD);
  bool wasSharedSkipInline(clang::OMPExecutableDirective *S, clang::VarDecl const *VD);
  bool wasFirstPrivateTask(clang::OMPExecutableDirective *S,
                           clang::VarDecl const *VD);
  bool emptyOrOnlyPrivate(clang::OMPExecutableDirective *S);

  clang::OMPExecutableDirective *
  getParent(clang::OMPExecutableDirective *);

  /// \brief Check if the directive is inline.
  ///
  /// An "inline" OpenMP directive in the meaning of this compiler, is a
  /// directive which doesn't require to generate an outlined function. For
  /// example, the critical directive is directly replaced inside the original
  /// source code.
  bool isInlineDirective(clang::OMPExecutableDirective *S);
};

#endif
