#ifndef KSTAR_KAAPIPRAGMA_H
#define KSTAR_KAAPIPRAGMA_H

#include <clang/AST/StmtOpenMP.h>
#include <clang/AST/ASTContext.h>
#include <clang/Basic/SourceManager.h>

#include <string>
#include <map>

#include "RuntimePragma.h"

#define KAAPI_DIRECTIVE(Name)                                                  \
  class Kaapi##Name : public Runtime##Name {                                   \
  public:                                                                      \
    Kaapi##Name(clang::OMP##Name##Directive *OMPD, clang::ASTContext &C,       \
                DataSharingInfo &DSI)                                          \
        : Runtime##Name(OMPD, C, DSI) {}                                       \
  }

class KaapiFor : public RuntimeFor {
public:
  KaapiFor(clang::OMPForDirective *S, clang::ASTContext &C,
           DataSharingInfo &DSI);

  std::string genReplacePragma(std::queue<std::string> &q) override;
};

// WARNING : source location is a pragma
// XXX: Get rid of this.
typedef std::map<clang::Type const *, clang::SourceLocation> MapFormat;

class KaapiTask : public RuntimeTask {
public:
  KaapiTask(clang::OMPTaskDirective *t, clang::ASTContext &c,
            DataSharingInfo &dsi);

  std::string genReplacePragma(std::queue<std::string> &q) override;
  std::string genBeforeFunction(std::queue<std::string> &q) override;
  std::string genAfterFunction(std::queue<std::string> &q) override;

protected:
  std::string genDeclareExtraArgStruct();

  /// \brief Emit a task wrapper function.
  std::string EmitTaskWrapper(std::queue<std::string> &);

  /// \brief Emit memory view parameters.
  std::string EmitMemoryViewParams(clang::VarDecl const *, int &);

  /// \brief Emit a task.
  std::string EmitTask();

  /// \brief Emit all task dependencies types
  std::string EmitTaskDependsType(clang::VarDecl const *);


  /// \brief Emit a format type name.
  std::string EmitFormatType(clang::VarDecl const *);

  /// \brief Return true if the type is a format builtin.
  bool isBuiltinFormatType(const clang::Type *);

  /// \brief Emit a format builtin type name.
  std::string EmitBuiltinFormatType(clang::Type const *);

  /// \brief Emit a format user type name.
  std::string EmitUserFormatType(clang::Type const *);

  /// \brief Emit a format user (including functions to get access to it).
  std::string EmitUserFormat(clang::Type const *);

  static MapFormat formatRegistered_;
};

class KaapiTarget : public RuntimeTarget {
public:
  KaapiTarget(clang::OMPTargetDirective *t, clang::ASTContext &c,
              DataSharingInfo &dsi);
  std::string genReplacePragma(std::queue<std::string> &) override;
  std::string genBeforeFunction(std::queue<std::string> &) override;
  std::string genAfterFunction(std::queue<std::string> &) override;
};

KAAPI_DIRECTIVE(Barrier);
KAAPI_DIRECTIVE(Critical);
KAAPI_DIRECTIVE(Sections);
KAAPI_DIRECTIVE(Master);
KAAPI_DIRECTIVE(Ordered);
KAAPI_DIRECTIVE(Taskwait);
KAAPI_DIRECTIVE(Flush);
KAAPI_DIRECTIVE(Taskgroup);
KAAPI_DIRECTIVE(Single);
KAAPI_DIRECTIVE(Taskyield);
KAAPI_DIRECTIVE(Parallel);
KAAPI_DIRECTIVE(ParallelFor);
KAAPI_DIRECTIVE(ParallelSections);

#endif
