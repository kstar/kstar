/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2016-2017  Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#include <starpu.h>
#include "../include/omp.h"

/*
 * Execution environment routines.
 */
void omp_set_num_threads(int num_threads)
{
  starpu_omp_set_num_threads(num_threads);
}

int omp_get_num_threads(void)
{
  return starpu_omp_get_num_threads();
}

int omp_get_max_threads(void)
{
  return starpu_omp_get_max_threads();
}

int omp_get_thread_num(void)
{
  return starpu_omp_get_thread_num();
}

int omp_get_num_procs(void)
{
  return starpu_omp_get_num_procs();
}

int omp_in_parallel(void)
{
  return starpu_omp_in_parallel();
}

void omp_set_dynamic(int dynamic_threads)
{
  starpu_omp_set_dynamic(dynamic_threads);
}

int omp_get_dynamic(void)
{
  return starpu_omp_get_dynamic();
}

int omp_get_cancellation(void)
{
  return starpu_omp_get_cancellation();
}

void omp_set_nested(int nested)
{
  starpu_omp_set_nested(nested);
}

int omp_get_nested(void)
{
  return starpu_omp_get_nested();
}

void omp_set_schedule(omp_sched_t kind, int chunk_size)
{
  starpu_omp_set_schedule((enum starpu_omp_sched_value)kind, chunk_size);
}

void omp_get_schedule(omp_sched_t *kind, int *chunk_size)
{
  starpu_omp_get_schedule((enum starpu_omp_sched_value *)kind, chunk_size);
}

int omp_get_thread_limit (void)
{
  return starpu_omp_get_thread_limit();
}

void omp_set_max_active_levels(int max_levels)
{
  starpu_omp_set_max_active_levels(max_levels);
}

int omp_get_max_active_levels(void)
{
  return starpu_omp_get_max_active_levels();
}

int omp_get_level(void)
{
  return starpu_omp_get_level();
}

int omp_get_ancestor_thread_num(int level)
{
  return starpu_omp_get_ancestor_thread_num(level);
}

int omp_get_team_size(int level)
{
  return starpu_omp_get_team_size(level);
}

int omp_get_active_level(void)
{
  return starpu_omp_get_active_level();
}

int omp_in_final(void)
{
  return starpu_omp_in_final();
}

omp_proc_bind_t omp_get_proc_bind(void)
{
 return (omp_proc_bind_t)starpu_omp_get_proc_bind();
}

int omp_get_num_places(void)
{
  return starpu_omp_get_num_places();
}

int omp_get_place_num_procs(int place_num)
{
  return omp_get_place_num_procs(place_num);
}

void omp_get_place_proc_ids(int place_num, int *ids)
{
  omp_get_place_proc_ids(place_num, ids);
}

int omp_get_place_num(void)
{
  return starpu_omp_get_place_num();
}

int omp_get_partition_num_places(void)
{
  return starpu_omp_get_partition_num_places();
}

void omp_get_partition_place_nums(int *place_nums)
{
  starpu_omp_get_partition_place_nums(place_nums);
}

void omp_set_default_device(int device_num)
{
  starpu_omp_set_default_device(device_num);
}

int omp_get_default_device(void)
{
  return starpu_omp_get_default_device();
}

int omp_get_num_devices(void)
{
  return starpu_omp_get_num_devices();
}

int omp_get_num_teams(void)
{
  return starpu_omp_get_num_teams();
}

int omp_get_team_num(void)
{
  return starpu_omp_get_team_num();
}

int omp_is_initial_device(void)
{
  return starpu_omp_is_initial_device();
}

int omp_get_initial_device(void)
{
  return starpu_omp_get_initial_device();
}

int omp_get_max_task_priority(void)
{
  return starpu_omp_get_max_task_priority();
}

/*
 * Lock routines.
 */
void omp_init_lock(omp_lock_t *lock)
{
  starpu_omp_init_lock((starpu_omp_lock_t *)lock);
}

void omp_destroy_lock(omp_lock_t *lock)
{
  starpu_omp_destroy_lock((starpu_omp_lock_t *)lock);
}

void omp_set_lock(omp_lock_t *lock)
{
  starpu_omp_set_lock((starpu_omp_lock_t *)lock);
}

void omp_unset_lock(omp_lock_t *lock)
{
  starpu_omp_unset_lock((starpu_omp_lock_t *)lock);
}

int omp_test_lock(omp_lock_t *lock)
{
  return starpu_omp_test_lock((starpu_omp_lock_t *)lock);
}

void omp_init_nest_lock(omp_nest_lock_t *lock)
{
  starpu_omp_init_nest_lock((starpu_omp_nest_lock_t *)lock);
}

void omp_destroy_nest_lock(omp_nest_lock_t *lock)
{
  starpu_omp_destroy_nest_lock((starpu_omp_nest_lock_t *)lock);
}

void omp_set_nest_lock(omp_nest_lock_t *lock)
{
  starpu_omp_set_nest_lock((starpu_omp_nest_lock_t *)lock);
}

void omp_unset_nest_lock(omp_nest_lock_t *lock)
{
  starpu_omp_unset_nest_lock((starpu_omp_nest_lock_t *)lock);
}

int omp_test_nest_lock(omp_nest_lock_t *lock)
{
  return starpu_omp_test_nest_lock((starpu_omp_nest_lock_t *)lock);
}

/*
 * Timing routines.
 */
double omp_get_wtime (void)
{
    return starpu_omp_get_wtime();
}

double omp_get_wtick (void)
{
    return starpu_omp_get_wtick();
}

starpu_pthread_mutex_t __kstar__mutex__;

__attribute__((constructor))
static void runtime_abi_constructor(void)
{
    if(starpu_omp_init() == -ENODEV)
        exit(-ENODEV);
    pthread_mutex_init(&__kstar__mutex__, 0);
}

__attribute__((destructor))
static void runtime_abi_destructor(void)
{
    pthread_mutex_destroy(&__kstar__mutex__);
    starpu_omp_shutdown();
}


// Compatibility for libgomp call support
void GOMP_atomic_start()
{
    starpu_omp_atomic_fallback_inline_begin();
}

void GOMP_atomic_end()
{
    starpu_omp_atomic_fallback_inline_end();
}

// Make sur library is linked even when as-needed is used (to call constructor/destructor)
int __kstar_extern_link_forced;

void* kstar_starpu_malloc(size_t size)
{
    void* A;
    starpu_malloc(&A, size);
    return A;
}

void __kmpc_begin(void *__unused_loc, int32_t __unused_flags)
{
}

void __kmpc_end(void *__unused_loc)
{
}
