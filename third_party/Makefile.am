
all-local: xkaapi-install.stamp starpu-install.stamp llvm-install.stamp $(BUILD_TARGETS)

BUILD_TARGETS=
 
EXTRA_DIST=llvm clang
dist-hook: xkaapi-configure.stamp
	$(MAKE) -C build-xkaapi distdir distdir=../$(distdir)/xkaapi
	$(MAKE) -C build-starpu distdir distdir=../$(distdir)/starpu
	chmod u+w $(distdir)/llvm $(distdir)/clang
	$(RM) -r $(distdir)/llvm/.git* $(distdir)/clang/.git*

# Workaround an unknown bug:
# without it, invoking make in parallel in this directory is ok
# but when run by the Makefile in .., an error is reported!
.NOTPARALLEL:

$(srcdir)/xkaapi/configure:
	cd $(srcdir)/xkaapi && ./bootstrap

xkaapi-configure.stamp: $(srcdir)/xkaapi/configure
	$(MKDIR_P) build-xkaapi
	cd build-xkaapi && \
	CC=$(CC) CXX=$(CXX) $(abs_srcdir)/xkaapi/configure \
		--disable-api-kaapixx \
		--disable-api-quark \
		--disable-api-kaapif \
		--enable-libkomp \
		--without-perfcounter \
		--without-poti \
		--without-papi \
		--without-cuda \
		--without-cupti \
		--with-numa=check \
		--disable-network \
		--prefix=$(pkglibdir)/kaapi
	touch $@

if KSTAR_COMPILE_KAAPI
KAAPI_PKGCONFIG_UNINSTALLED_FILES=\
	install/kaapi/lib/pkgconfig/komp-uninstalled.pc \
	install/kaapi/lib/pkgconfig/kaapi-uninstalled.pc

BUILD_TARGETS+=$(KAAPI_PKGCONFIG_UNINSTALLED_FILES)

PHONY: .FORCE_KAAPI_BUILD
.FORCE_KAAPI_BUILD:
xkaapi-build.stamp: xkaapi-configure.stamp .FORCE_KAAPI_BUILD
	if ! $(MAKE) -C build-xkaapi -q; then \
		$(MAKE) -C build-xkaapi ; \
		touch xkaapi-built.stamp ; \
	else \
		echo "Nothing to be rebuilt"; \
	fi
	touch $@

xkaapi-install.stamp: xkaapi-build.stamp
	if test -f xkaapi-built.stamp ; then \
		$(MAKE) -C build-xkaapi -j1 install DESTDIR=$(abs_builddir)/install/kaapi-install && \
		$(LN_S) -f kaapi-install/$(pkglibdir)/kaapi install/ && \
		$(RM) install/kaapi/lib/*.la && \
		$(RM) xkaapi-built.stamp ;\
	else \
		echo "Skipping installation as no rebuilt has been done"; \
	fi
	touch $@

$(KAAPI_PKGCONFIG_UNINSTALLED_FILES): \
%-uninstalled.pc: %.pc xkaapi-install.stamp
	$(SED) < $< > $@ 's|^prefix=|prefix=$(abs_builddir)/install/kaapi-install|'

install-exec-hook-xkaapi:
	$(MKDIR_P) "$(DESTDIR)$(pkglibdir)/kaapi/lib/pkgconfig"
	set -e ; \
	for f in install/kaapi/lib/libkaapi.* install/kaapi/lib/libkomp.* ; do \
		$(install_sh_DATA) "$$f" "$(DESTDIR)$(pkglibdir)/kaapi/lib/" ; \
	done
	set -e ; \
	for f in install/kaapi/lib/pkgconfig/kaapi.pc install/kaapi/lib/pkgconfig/komp.pc ; do \
		$(install_sh_DATA) "$$f" "$(DESTDIR)$(pkglibdir)/kaapi/lib/pkgconfig/" ; \
	done
	
install-data-hook-xkaapi:
	$(MKDIR_P) "$(DESTDIR)$(pkglibdir)/kaapi/include"
	set -e ; \
	for f in install/kaapi/include/* ; do \
		$(install_sh_DATA) "$$f" "$(DESTDIR)$(pkglibdir)/kaapi/include/" ; \
	done

uninstall-hook-xkaapi:
	$(RM) -r "$(DESTDIR)$(pkglibdir)/kaapi"
else
xkaapi-install.stamp:
	touch $@
install-exec-hook-xkaapi:
install-data-hook-xkaapi:
uninstall-hook-xkaapi:
endif

$(srcdir)/starpu/configure:
	cd $(srcdir)/starpu && ./autogen.sh

starpu-configure.stamp: $(srcdir)/starpu/configure
	$(MKDIR_P) build-starpu
	cd build-starpu && \
	CC=$(CC) CXX=$(CXX) $(abs_srcdir)/starpu/configure \
    --enable-openmp \
    --without-fxt \
    --disable-allocation-cache \
    --disable-ayudame1 \
    --disable-ayudame2 \
    --disable-build-doc \
    --disable-build-examples \
    --disable-build-tests \
    --disable-cuda \
    --disable-debug \
    --disable-fortran \
    --disable-gcc-extensions \
    --disable-mic \
    --disable-mlr \
    --disable-mpi \
    --disable-opencl \
    --disable-rcce \
    --disable-simgrid \
    --disable-starpu-top \
    --disable-starpufft \
    --disable-valgrind \
    --disable-verbose \
    --enable-blas-lib=none \
    --enable-maxbuffers=32 \
		--prefix=$(pkglibdir)/starpu
	touch $@

if KSTAR_COMPILE_STARPU
PHONY: .FORCE_STARPU_BUILD
.FORCE_STARPU_BUILD:
starpu-build.stamp: starpu-configure.stamp .FORCE_STARPU_BUILD
	if ! $(MAKE) -C build-starpu -q; then \
		$(MAKE) -C build-starpu ; \
		touch starpu-built.stamp ; \
	else \
		echo "Nothing to be rebuilt"; \
	fi
	touch $@

starpu-install.stamp: starpu-build.stamp
	set -e; \
	if test -f starpu-built.stamp ; then \
		$(MAKE) -C build-starpu -j1 install DESTDIR=$(abs_builddir)/install/starpu-install; \
		$(LN_S) -f starpu-install/$(pkglibdir)/starpu install/; \
		$(RM) install/starpu/lib/*.la; \
		cp install/starpu/lib/pkgconfig/starpu-1.3.pc install/starpu/lib/pkgconfig/starpu-1.3-install.pc; \
		$(SED) -i 's|^prefix=|prefix=$(abs_builddir)/install/starpu-install|' install/starpu/lib/pkgconfig/starpu-1.3.pc; \
		$(RM) starpu-built.stamp ;\
	else \
		echo "Skipping installation as no rebuilt has been done"; \
	fi
	touch $@

install-exec-hook-starpu:
	$(MKDIR_P) "$(DESTDIR)$(pkglibdir)/starpu/lib/pkgconfig"
	set -e ; \
	for f in install/starpu/lib/libstarpu-*; do \
		$(install_sh_DATA) "$$f" "$(DESTDIR)$(pkglibdir)/starpu/lib/" ; \
	done
	set -e ; \
	$(install_sh_DATA) install/starpu/lib/pkgconfig/starpu-1.3-install.pc "$(DESTDIR)$(pkglibdir)/starpu/lib/pkgconfig/starpu-1.3.pc"

install-data-hook-starpu:
	$(MKDIR_P) "$(DESTDIR)$(pkglibdir)/starpu/include/starpu/1.3"
	set -e ; \
	for f in install/starpu/include/starpu/1.3/* ; do \
		$(install_sh_DATA) "$$f" "$(DESTDIR)$(pkglibdir)/starpu/include/starpu/1.3/" ; \
	done

uninstall-hook-starpu:
	$(RM) -r "$(DESTDIR)$(pkglibdir)/starpu"
else
starpu-install.stamp:
	touch $@
install-exec-hook-starpu:
install-data-hook-starpu:
uninstall-hook-starpu:
endif

install-exec-hook: install-exec-hook-xkaapi install-exec-hook-starpu
install-data-hook: install-data-hook-xkaapi install-data-hook-starpu
uninstall-hook: uninstall-hook-xkaapi uninstall-hook-starpu
distclean-local: distclean-local-xkaapi distclean-local-starpu
distclean-local-xkaapi:
	$(RM) -r xkaapi-*.stamp build-xkaapi
	$(RM) -r install/kaapi install/kaapi-install
	-rmdir install
distclean-local-starpu:
	$(RM) -r starpu-*.stamp build-starpu
	$(RM) -r install/starpu install/starpu-install
	-rmdir install

if KSTAR_COMPILE_LLVM

if KSTAR_ENABLE_LLVM_DEBUG
LLVM_CMAKE_OPTS = \
	-DCMAKE_BUILD_TYPE=Debug
else
LLVM_CMAKE_OPTS = \
	-DCMAKE_BUILD_TYPE=Release
endif

LLVM_CMAKE_OPTS += \
	-DCMAKE_INSTALL_PREFIX=$(pkglibdir)/llvm \
	-DLLVM_TARGETS_TO_BUILD="X86;AArch64" \
	-DLLVM_EXTERNAL_CLANG_SOURCE_DIR=$(abs_srcdir)/clang

if KSTAR_WITH_GCC_TOOLCHAIN
LLVM_CMAKE_OPTS += \
	-DCMAKE_C_COMPILER=$(CC) \
	-DCMAKE_CXX_COMPILER=$(CXX) \
	-DCMAKE_EXE_LINKER_FLAGS="-L$(KSTAR_GCC_TOOLCHAIN)/lib64 -Wl,-rpath,$(KSTAR_GCC_TOOLCHAIN)/lib64" \
	-DGCC_INSTALL_PREFIX=$(KSTAR_GCC_TOOLCHAIN)
endif

llvm-cmake.stamp:
	$(MKDIR_P) build-llvm
	cd build-llvm && \
	CC=$(CC) CXX=$(CXX) cmake $(abs_srcdir)/llvm $(LLVM_CMAKE_OPTS)
	touch $@

llvm-build.stamp: llvm-cmake.stamp
	$(MAKE) -C build-llvm
	touch $@

llvm-install.stamp: llvm-build.stamp
	$(MAKE) -C build-llvm install DESTDIR=$(abs_builddir)/install/llvm-install
	$(LN_S) -f llvm-install/$(pkglibdir)/llvm install/
	touch $@


install-exec-hook-llvm:
	set -e ; \
	base="install/llvm/lib/clang/$$(install/llvm/bin/llvm-config --version)" ; \
	for d in $$(find "$$base" -type d -printf "%P\n") ; do \
		$(MKDIR_P) "$(DESTDIR)$(pkglibdir)/clang/$$d" ; \
	done ; \
	for f in $$(find "$$base" -type f -printf "%P\n") ; do \
		$(install_sh_DATA) "$$base/$$f" "$(DESTDIR)$(pkglibdir)/clang/$$(dirname $$f)" ; \
	done

uninstall-hook-llvm:
	$(RM) -r "$(DESTDIR)$(pkglibdir)/clang"
else
llvm-install.stamp:
	touch $@
install-exec-hook-llvm:
uninstall-hook-llvm:
endif

install-exec-hook: install-exec-hook-llvm
uninstall-hook: uninstall-hook-llvm
distclean-local: distclean-local-llvm
distclean-local-llvm:
	$(RM) -r llvm-*.stamp build-llvm
	$(RM) -r install/llvm install/llvm-install
	-rmdir install

