#include <iostream>
#include <string>
#include <assert.h>

using namespace std;

static int compare_double(const double f1, const double f2)
{
    double precision = 0.00001;
    return ((f1 - precision < f2) && (f1 + precision) > f2);
}

// An useless function template which no specializations
// (do not need to be parsed)
template <class T>
static T useless(T x)
{
    T res;
#pragma omp parallel num_threads(1)
    res =  (x > 0) ? -x : x;
    return res;
}

// A single template example.
template <class T>
inline T square(T x)
{
    T res;
#pragma omp parallel for
    for (int i = 0; i < 10; i++)
        res = x * x;
    return res;
}

static void test_single_template()
{
    int    i = 2;
    float  x = 2.2;
    double y = 2.2000001;

    if (square<int>(i) != 4)
        abort();
    if (!compare_double(square<float>(x), 4.84))
        abort();
    // Explicit use of template
    if (!compare_double(square<double>(y), 4.84))
        abort();
    // Implicit use of template
    if (!compare_double(square(y), 4.84))
        abort();
}

// A template specialization example.
template <>
string square<string>(string ss)
{
    int useless;
#pragma omp parallel for
    for (int i = 0; i < 10; i++)
        useless = i * i;
    (void)useless;
    return (ss+ss);
}

static void test_specialization_template()
{
    string ww("Aaa");
    if (square<string>(ww) != "AaaAaa")
        abort();
}

// A multiple templated types example.
template <typename T, typename U>
void squareAndPrint(T x, U y)
{
    T result;
    U otherVar;

#pragma omp parallel for
    for (int i = 0; i < 10; i++)
        result = x * x;
#pragma omp parallel for
    for (int i = 0; i < 10; i++)
        otherVar = y * y;

    if (result != 4)
        abort();
    if (!compare_double(otherVar, 4.41))
        abort();
}

static void test_multiple_templated_types()
{
    int   ii = 2;
    float jj = 2.1;

    squareAndPrint<int,float>(ii, jj);
}

// A non-type parameters example.
template <typename T, int count>
void loopIt(T x)
{
    T val[count];

#pragma omp parallel for num_threads(1)
    for (int ii = 0; ii < count; ii++) {
#pragma omp critical
        val[ii] = x++;
    }

    if (!compare_double(val[0], 2.1) ||
        !compare_double(val[1], 3.1) ||
        !compare_double(val[2], 4.1))
        abort();
}

static void test_non_type_parameters()
{
    float xx = 2.1;

    loopIt<float,3>(xx);
}

int main ()
{
    test_single_template();
    test_specialization_template();
    test_multiple_templated_types();
    test_non_type_parameters();
    return 0;
}
