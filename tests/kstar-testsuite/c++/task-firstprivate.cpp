#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <omp.h>

struct foo {
    int a;
    int b;
};

int main()
{
    struct foo bar[2];
    bar[0].a = 12;
    bar[0].b = 16;
    int res = 0;
#pragma omp parallel
    #pragma omp task firstprivate(bar)
    {
        if (omp_get_thread_num() == 0) {
            bar[0].a = 11;
            res |= (omp_get_thread_num() >= omp_get_num_threads());
            res |= ((bar[0].a - bar[0].b) != -5);
        } else {
            res |= (omp_get_thread_num() >= omp_get_num_threads());
            res |= ((bar[0].a - bar[0].b) != -4);
        }
    }
    if (res != 0)
        abort();
    return 0;
}
