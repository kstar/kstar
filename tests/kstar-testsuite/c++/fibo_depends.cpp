#include <iostream>
#include <cstdlib>

void fibonacci(long *result,  const long n)
{
    if (n<2)
        *result = n;
    else
    {
#pragma kaapi alloca(r1,r2)
        long r1,r2;
        /* long* _kaapi_r1 = kaapi_alloca(sizeof(long); long& r1 = *_kaapi_r1 */
#pragma omp task shared(r1) depend(out:r1) firstprivate(n)
        fibonacci(&r1, n-1 );
#pragma omp task shared(r2) depend(out:r2) firstprivate(n)
        fibonacci(&r2, n-2 );

#pragma omp task shared(r1,r2) depend(in: r1, r2) depend(out: result)
        *result = r1 + r2;
#pragma omp taskwait
    }
}

static void check_result(const int n, const long *result)
{
/*#pragma omp task*/
    if (n != 10 || *result != 55)
        abort();
}

int main( int argc, char** argv)
{
    long result;
    int m;
    if (argc >1) m = atoi(argv[1]);
    else m = 10;
    {
#pragma omp parallel
#pragma omp single
        {
#pragma omp task shared(result) depend(out:result) depend(in:m)
            fibonacci(&result, m);
#pragma omp taskwait
            /*#pragma omp task*/
#pragma omp task depend(in:result, m)
            check_result(m, &result);
        }
    }
    return 0;
}
