#include <stdlib.h>

int
main (void)
{
  int i[2];
  i[0] = 4;
  i[1] = 6;
#pragma omp single copyprivate (i)
  {
    i[0] = 6;
    i[1] = 4;
  }
  if (i[0] != 6 || i[1] != 4)
    abort ();
  return 0;
}
