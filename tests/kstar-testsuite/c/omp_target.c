#include <stdio.h>
#include <stdlib.h>

void _gpu_increase(float *);
void _cpu_increase(float *);

static void init(float *v1, float *v2, int len)
{
  int i;

  for (i = 0; i < len; ++i) {
    v1[i] = 2;
    v2[i] = 3;
  }
}

static void output(float *p, int len)
{
  int i;

  for (i = 0; i < len; i++)
    printf("%f ", p[i]);
  printf("\n");
}

#pragma omp declare target
extern void increase(float *arr);
#pragma omp end declare target

#define N 10

void _cpu_increase(float *arr)
{
  arr[0] = arr[0] + 1.0f;
	arr[2] = arr[2] + 1.0f;
}

int main()
{
  float v1[N];
  float *v2 = malloc(N * sizeof(float));
  init(v1, v2, N);

#pragma omp parallel
#pragma omp master
#pragma omp target map(tofrom: v1, v2[0:N])
  increase(v2);
  output(v2, N);

  free(v2);
  return 0;
}
