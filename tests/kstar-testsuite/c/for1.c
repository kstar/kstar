#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static int randomvalue;

int main()
{
    do  {
      randomvalue = rand()%10000;
    } while (randomvalue == 0);

    int a = randomvalue;
    #pragma omp parallel 
    #pragma omp for private(a)
      for (int i=0; i<100; ++i)
      {
        a +=  i; 
      }
    assert( a == randomvalue );
    return 0;
}
