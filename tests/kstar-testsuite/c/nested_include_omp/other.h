#include <omp.h>

int bar(int i)
{
    #pragma omp parallel num_threads(i)
    #pragma omp master
        i += omp_get_num_threads();
    return i;
}
