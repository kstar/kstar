#include <stdlib.h>
#include <omp.h>

static int i = 2;
#pragma omp threadprivate(i)

int main()
{
#pragma omp parallel
  i = omp_get_thread_num();

  /* do not use a combined 'parallel for' here, otherwise omp_get_num_threads() gets evaluated in the context of the enclosing region */
#pragma omp parallel
#pragma omp for schedule(static, 1)
  for(int j=0; j<omp_get_num_threads(); j++)
    if(j != i)
      abort();
  return 0;
}
