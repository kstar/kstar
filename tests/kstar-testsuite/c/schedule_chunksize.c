#include <stdlib.h>
#include <omp.h>

static int i = 2;
#pragma omp threadprivate(i)

int main()
{
  const int chunksize = 1;

#pragma omp parallel
  i = omp_get_thread_num();

#pragma omp parallel for schedule(static, chunksize)
  for(int j=0; j<omp_get_num_threads(); j++)
    if(j != i)
      abort();
  return 0;
}
